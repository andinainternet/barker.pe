<?php  $barker_home = home_url(); ?>
<?php if (!is_user_logged_in()): ?>
<?php
    wp_redirect($barker_home . '/cuenta');
     //auth_redirect();
     exit;
    ?>
<?php else: ?>
<?php
    get_header();
    
    ?>
<?php $barker_suscripcion_productos = get_field('barker_suscripcion_productos'); ?>
<!-- Code Here -->
<div class="page-wrap">
    <main class="main">
        <div class="container">
            <form class="form form--big" id="form-sucription" enctype="multipart/form-data" action="" method="post">
                <div id="delivery-wizard">
                    <!-- Step 1-->
                    <h3>
                        <span class="icon">
                            <svg>
                                <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#step-dog"></use>
                            </svg>
                        </span>
                        <span class="text">Perfil</span>
                    </h3>
                    <section>
                        <div class="row justify-content-center">
                            <div class="col-lg-9">
                                <div class="u-text-center">
                                    <div class="subtitle-xs">
                                        <h5>¡Bienvenido al espacio de suscripciones Barker!</h5>
                                    </div>
                                    <div class="text-line">
                                        <p>Estás cada vez más cerca de darle a tu mascota lo que quiere y necesita. Sigue los pasos para realizar una suscripción Barker.</p>
                                        <p>Actualmente contamos con entregas a: Barranco, Chorrillos, Jesus Maria, Lince, La Molina, Miraflores, San Borja, San Isidro, San Miguel, Surco, Surquillo.</p>
                                        <p><span class="text-black">¿No encuentras tu distrito? Por favor déjanos</span><a class="link js-add-district-popup" href="">¡acá tu información!</a></p>
                                    </div>
                                    <div class="district-add popup-inner js-add-district">
                                        <div class="u-text-center">
                                            <div class="subtitle-xs">
                                                <h3>¿No encuentras tu distrito?</h3>
                                            </div>
                                            <p class="text-black">Por favor déjanos acá tu información</p>
                                        </div>
                                        <div class="form">
                                            <div class="field-wrapper">
                                                <input type="text" name="f-email-interested" id="f-email-interested" placeholder="Correo electrónico">
                                            </div>
                                            <div class="field-wrapper">
                                                <select class="js-add-district-select" name="f-district-search-add" id="f-district-search-add">
                                                    <option value=""></option>
                                                    <option value="Villa el Salvador">Villa el Salvador</option>
                                                    <option value="Villa María del Triunfo">Villa María del Triunfo</option>
                                                </select>
                                            </div>
                                            <div class="field-action u-flex justify-content-center">
                                                <button class="btn btn--secondary" type="button">Cancelar</button>
                                                <button class="btn btn--primary" type="button">Enviar</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-line">
                                        <p class="text-black">Solo ayúdanos con los siguientes datos.</p>
                                    </div>
                                </div>
                                <div class="field-wrapper field-wrapper--highlight name-dog">
                                    <label for="f-dog-name">¿Cómo se llama tu perro?</label>
                                    <input type="text" name="f-dog-name" id="f-dog-name" placeholder="Ejemplo: Boby" autocomplete="off">
                                </div>
                                <div class="field-wrapper field-wrapper--highlight">
                                    <label>Sube la foto de <span class="js-dog-name">tu mascota</span></label>
                                    <p class="note u-text-center">(La foto es opcional)</p>
                                    <div class="file-upload">
                                        <div class="file-upload__preview">
                                            <figure><img src="<?php echo get_template_directory_uri(); ?>/assets/images/avatar-dog.png" alt=""></figure>
                                        </div>
                                        <div class="file-upload__field">
                                            <label class="btn btn--primary" for="f-input-file">Subir foto de <span class="js-dog-name">tu mascota</span></label>
                                            <input class="js-upload-hidden" type="file" name="f-input-file" id="f-input-file">
                                        </div>
                                    </div>
                                    <div class="feedback u-text-center">
                                        <p>Recuerda que el tamaño máximo de la imagen debe ser 8MB</p>
                                    </div>
                                </div>
                                <div class="field-wrapper field-wrapper--highlight">
                                    <label>¿Cuál es el sexo de <span class="js-dog-name">tu mascota</span>?</label>
                                    <div class="radio-group radio-group--square">
                                        <div class="radio">
                                            <input type="radio" name="f-dog-sex" id="f-male" value="Macho">
                                            <label for="f-male">Macho</label>
                                        </div>
                                        <div class="radio">
                                            <input type="radio" name="f-dog-sex" id="f-female" value="Hembra">
                                            <label for="f-female">Hembra</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="field-wrapper field-wrapper--highlight filter-female">
                                    <label>Para un mejor filtro ayúdanos con las<br>siguientes opciones</label>
                                    <div class="radio-group radio-group--full">
                                        <div class="radio">
                                            <input type="radio" name="f-status-female" id="f-pregnat" value="está preñada">
                                            <label for="f-pregnat">
                                                <div class="radio__content">
                                                    <div class="radio__text">
                                                        <p><span class="js-dog-name">tu mascota</span>, está preñada</p>
                                                    </div>
                                                </div>
                                            <label class="radio__icon" for="f-pregnat"></label>
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <input type="radio" name="f-status-female" id="f-has-pups" value="tiene crías">
                                            <label for="f-has-pups">
                                                <div class="radio__content">
                                                    <div class="radio__text">
                                                        <p><span class="js-dog-name">tu mascota</span>, tiene crías</p>
                                                    </div>
                                                </div>
                                            <label class="radio__icon" for="f-has-pups"></label>
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <input type="radio" name="f-status-female" id="f-status-na" value="na">
                                            <label for="f-status-na">
                                                <div class="radio__content">
                                                    <div class="radio__text">
                                                        <p>Ninguna de las anteriores</p>
                                                    </div>
                                                </div>
                                            <label class="radio__icon" for="f-status-na"></label>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- Step 2-->
                    <h3>
                        <span class="icon">
                            <svg>
                                <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#step-data"></use>
                            </svg>
                        </span>
                        <span class="text">Datos</span>
                    </h3>
                    <section>
                        <div class="row justify-content-center">
                            <div class="col-lg-10">
                                <div class="field-wrapper field-wrapper--highlight">
                                    <label>¿Cuál es la edad de <span class="js-dog-name">tu mascota</span>?</label>
                                    <div class="radio-group radio-group--full row">
                                        <div class="radio radio--small col-md-4">
                                            <input type="radio" name="f-dog-age" id="f-puppy" value="cachorro">
                                            <label for="f-puppy">
                                                <div class="radio__content">
                                                    <div class="radio__image"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/dog-puppy.png" alt=""></div>
                                                    <div class="radio__text">
                                                        <h6>Cachorro</h6>
                                                        <p>0 a 1 año y medio</p>
                                                    </div>
                                                </div>
                                            <label class="radio__icon" for="f-puppy"></label>
                                            </label>
                                        </div>
                                        <div class="radio radio--small col-md-4">
                                            <input type="radio" name="f-dog-age" id="f-adult" value="adulto">
                                            <label for="f-adult">
                                                <div class="radio__content">
                                                    <div class="radio__image"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/dog-adult.png" alt=""></div>
                                                    <div class="radio__text">
                                                        <h6>Adulto</h6>
                                                        <p>2 a 6 años</p>
                                                    </div>
                                                </div>
                                            <label class="radio__icon" for="f-adult"></label>
                                            </label>
                                        </div>
                                        <div class="radio radio--small col-md-4">
                                            <input type="radio" name="f-dog-age" id="f-older-adult" value="adulto mayor">
                                            <label for="f-older-adult">
                                                <div class="radio__content">
                                                    <div class="radio__image"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/dog-older-adult.png" alt=""></div>
                                                    <div class="radio__text">
                                                        <h6>Adulto mayor</h6>
                                                        <p>7 a más años</p>
                                                    </div>
                                                </div>
                                            <label class="radio__icon" for="f-older-adult"></label>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="field-wrapper field-wrapper--highlight">
                                    <label for="f-dog-name">¿Cúal es el peso de <span class="js-dog-name">tu mascota</span>?</label><span class="feedback">Elige entre las tres categorías de peso y usa las razas como ejemplo para guiarte</span>
                                    <div class="row justify-content-center">
                                        <div class="col-lg-11">
                                            <div class="size-dog">
                                                <div class="size-dog__result">
                                                    <h5 class="js-dog-weight"></h5>
                                                    <span class="js-dog-race"></span>
                                                    <figure class="js-dog-image"><img src="" alt=""></figure>
                                                </div>
                                                <div class="size-dog__slider">
                                                    <div class="size-dog__less js-dog-less"><img src="" alt=""></div>
                                                    <div class="size-dog__higher js-dog-higher"><img src="" alt=""></div>
                                                    <div id="slider-size-dog"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="field-wrapper field-wrapper--highlight">
                                    <label for="f-dog-name">Selecciona el nivel de actividad de <span class="js-dog-name">tu mascota</span></label>
                                    <div class="radio-group radio-group--full row">
                                        <div class="radio radio--small col-md-4">
                                            <input type="radio" name="f-level-activity" id="f-low" value="bajo">
                                            <label for="f-low">
                                                <div class="radio__content">
                                                    <div class="radio__image"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/dog-bajo.png" alt=""></div>
                                                    <div class="radio__text">
                                                        <h6>Bajo</h6>
                                                    </div>
                                                </div>
                                            <label class="radio__icon" for="f-low"></label>
                                            </label>
                                        </div>
                                        <div class="radio radio--small col-md-4">
                                            <input type="radio" name="f-level-activity" id="f-medium" value="medio">
                                            <label for="f-medium">
                                                <div class="radio__content">
                                                    <div class="radio__image"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/dog-medio.png" alt=""></div>
                                                    <div class="radio__text">
                                                        <h6>Medio</h6>
                                                    </div>
                                                </div>
                                            <label class="radio__icon" for="f-medium"></label>
                                            </label>
                                        </div>
                                        <div class="radio radio--small col-md-4">
                                            <input type="radio" name="f-level-activity" id="f-high" value="alto">
                                            <label for="f-high">
                                                <div class="radio__content">
                                                    <div class="radio__image"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/dog-alto.png" alt=""></div>
                                                    <div class="radio__text">
                                                        <h6>Alto</h6>
                                                    </div>
                                                </div>
                                            <label class="radio__icon" for="f-high"></label>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- Step 3-->
                    <h3>
                        <span class="icon">
                            <svg>
                                <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#step-suscription"></use>
                            </svg>
                        </span>
                        <span class="text">Suscripción</span>
                    </h3>
                    <section>
                        <div class="row justify-content-center">
                            <div class="col-md-6 col-lg-5 u-hidden-tablet">
                                <div class="delivery-flavors is-empty">
                                    <div class="delivery-flavors__feedback u-text-center">
                                        <h2>Aquí se mostrará el detalle de tus sabores seleccionados</h2>
                                    </div>
                                    <div class="delivery-flavors__carousel">
                                    	<div class="owl-carousel owl-loaded owl-drag">
                                        <!-- Carrousel Description -->
                                        <?php if( $posts ): ?>
                                        <?php foreach( $barker_suscripcion_productos as $post): // variable must be called $post (IMPORTANT) ?>
                                        <?php setup_postdata($post); ?>
                                        <?php $barker_icon_type = $post->post_name; ?>
                                        <!-- CodeHere -->
                                        <div id="barkerDescription-<?php echo $barker_icon_type; ?>" class="owl-stage-outer barker-suscription-item_description__disable">
                                            <div class="owl-stage">
                                                <div class="owl-item">
                                                    <div class="delivery-flavor">
                                                        <div class="delivery-flavor__title subtitle-sm">
                                                            <h3><?php the_title(); ?></h3>
                                                        </div>
                                                        <div class="delivery-flavor__content">
                                                            <?php the_content() ?>
                                                        </div>
                                                        <div class="delivery-flavor__cta">
                                                            <a class="btn btn--primary btn--block" href="#">Ver cuadro nutricional</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; ?>
                                        <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                                        <?php endif; ?>	
                                        </div>												
                                        <!-- </div> -->
                                        <!-- <div class="owl-carousel js-delivery-flavors-carousel">
                                            </div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-7">
                                <!-- ////////////////////////////////////////////////// -->
                                <!-- Start Select Product-->
                                <!-- ////////////////////////////////////////////////// -->
                                <div class="delivery-filters">
                                    <div class="filter-by-flavors">
                                        <div class="subtitle-xs">
                                            <h4>Selecciona el sabor para <span class="js-dog-name">tu mascota</span></h4>
                                        </div>
                                        <p class="feedback"><span class="js-dog-name">tu mascota</span> necesita un estimado de <span class="js-kg-stimated"></span> kilogramos al día.</p>
                                        <!-- Sabores-->
                                        <div class="checkbox-group checkbox-group--circle">
                                            <!-- Circle Checkbox-->
                                            <?php if( $posts ): ?>
                                            <?php foreach( $barker_suscripcion_productos as $post): // variable must be called $post (IMPORTANT) ?>
                                            <?php setup_postdata($post); ?>
                                            <?php  
                                                $barker_icon_path = $barker_home . '/wp-content/themes/barker-theme/assets/images/icons/';
                                                $barker_icon_type = $post->post_name;
                                                $barker_icon_url =  $barker_icon_path . $barker_icon_type . '-icon.png';
                                                
                                                ?>
                                            <!-- CodeHere -->
                                            <div class="checkbox">
                                                <input type="radio" name="f-flavors" id="f-<?php echo $barker_icon_type; ?>-flavor" value="<?php echo $post->post_name;?>" data-flavor="<?php echo $post->post_name;?>" data-id="<?php the_ID();?>">
                                                <label for="f-<?php echo $barker_icon_type; ?>-flavor">
                                                    <div class="checkbox__content">
                                                        <div class="checkbox__image">
                                                            <img src="<?php echo $barker_icon_url; ?>" alt="">
                                                        </div>
                                                        <div class="checkbox__text">
                                                            <p><?php the_title(); ?></p>
                                                        </div>
                                                    </div>
                                                <label class="checkbox__icon" for="f-chicken-flavor"></label>
                                                </label>
                                            </div>
                                            <?php endforeach; ?>
                                            <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                                            <?php endif; ?>		
                                        </div>
                                    </div>
                                    <hr class="line-dotted line-dotted--yellow">
                                    <!-- ////////////////////////////////////////////////// -->
                                    <!-- Start Price Product-->
                                    <!-- ////////////////////////////////////////////////// -->
                                    <?php if( $posts ): ?>
                                    <?php foreach( $barker_suscripcion_productos as $post): // variable must be called $post (IMPORTANT) ?>
                                    <?php setup_postdata($post); ?>
                                    <?php 
                                        global $woocommerce;
                                        global $product;
                                        $barker_currency = get_woocommerce_currency_symbol();
                                        $barker_price = get_post_meta( get_the_ID(), '_regular_price', true);
                                        $barker_sale = get_post_meta( get_the_ID(), '_sale_price', true);
                                        $barker_icon_type = $post->post_name;
                                        ?>
                                    <!-- Start PriceBox -->
                                    <div id="<?php echo 'barkerPriceBox-' . $barker_icon_type; ?>" class="filter-by-time barker-suscription-item_priceBox__disable">
                                        <div class="subtitle-xs">
                                            <h4>Elige tu suscripción</h4>
                                        </div>
                                        <div class="radio-group radio-group--check justify-content-center">
                                            <div class="radio column">
                                                <?php 
                                                    if($product->product_type=='variable') {
                                                                    
                                                        $available_variations = $product->get_available_variations(); 
                                                    	$count = count($available_variations)-5;
                                                    	$variation_id=$available_variations[$count]['variation_id']; // Getting the variable id of just the 1st product. You can loop $available_variations to get info about each variation.
                                                    	?>
                                                <input type="radio" name="f-suscription-time" id="f-one-week-<?php echo $barker_icon_type; ?>" value="1 semana" data-variation="<?php echo $variation_id;?>" data-quantity="1">
                                                <label for="f-one-week-<?php echo $barker_icon_type; ?>">
                                                    <div class="radio__content">
                                                        <div class="radio__text">
                                                            <span class="title">Barker por una semana</span>
                                                            <!-- CodeHere -->
                                                            <ul class="price">
                                                                <li>
                                                                    <span>
                                                                    <?php 
                                                                        //Controla el orden del array para ver el precio de la variación
                                                                        // echo $count;
                                                                        // echo "<br/>";
                                                                        // var_dump($count);
                                                                        // var_dump($available_variations);
                                                                                       
                                                                               $variable_product1= new WC_Product_Variation( $variation_id );
                                                                               
                                                                               $regular_price = $variable_product1 ->regular_price;
                                                                               
                                                                               $sales_price = $variable_product1 ->sale_price;
                                                                               
                                                                               echo $barker_currency;
                                                                               echo $regular_price;
                                                                               }
                                                                           ?>	
                                                                    </span>
                                                                    <span>Por semana</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                <label class="radio__icon" for="f-one-month">
                                                    <span>
                                                        <svg>
                                                            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#check"></use>
                                                        </svg>
                                                    </span>
                                                </label>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="radio-group radio-group--check owl-carousel js-suscription-type-carousel">
                                            <div class="radio">
                                                <?php 
                                                    if($product->product_type=='variable') {
                                                                    
                                                        $available_variations = $product->get_available_variations(); 
                                                    	$count = count($available_variations)-4;
                                                    	$variation_id=$available_variations[$count]['variation_id']; // Getting the variable id of just the 1st product. You can loop $available_variations to get info about each variation.
                                                    	?>                                                
                                                <input type="radio" name="f-suscription-time" id="f-one-month-<?php echo $barker_icon_type; ?>" value="1 mes" data-variation="<?php echo $variation_id;?>" data-quantity="1">
                                                <label for="f-one-month-<?php echo $barker_icon_type; ?>">
                                                    <div class="radio__content">
                                                        <div class="radio__text">
                                                            <span class="title">Barker x 1 mes</span>
                                                            <ul class="price">
                                                                <li><span>
                                                                    <?php 
                                                                        $variable_product1= new WC_Product_Variation( $variation_id );
                                                                        
                                                                        $regular_price = $variable_product1 ->regular_price;
                                                                        
                                                                        $sales_price = $variable_product1 ->sale_price;
                                                                        
                                                                        echo $barker_currency;
                                                                        echo $regular_price;
                                                                        }
                                                                        ?>	
                                                                    </span><span>Por comida</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                <label class="radio__icon" for="f-one-month">
                                                    <span>
                                                        <svg>
                                                            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#check"></use>
                                                        </svg>
                                                    </span>
                                                </label>
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <?php 
                                                    if($product->product_type=='variable') {
                                                                    
                                                        $available_variations = $product->get_available_variations(); 
                                                    	$count = count($available_variations)-3;
                                                    	$variation_id=$available_variations[$count]['variation_id']; // Getting the variable id of just the 1st product. You can loop $available_variations to get info about each variation.
                                                    	?>                                                    	
                                                <input type="radio" name="f-suscription-time" id="f-three-months-<?php echo $barker_icon_type; ?>" value="3 meses" data-variation="<?php echo $variation_id;?>" data-quantity="1">
                                                <label for="f-three-months-<?php echo $barker_icon_type; ?>">
                                                    <div class="radio__content">
                                                        <div class="radio__text">
                                                            <span class="title">Barker 3 meses</span>
                                                            <ul class="price">
                                                                <li><span>
                                                                    <?php 
                                                                        $variable_product1= new WC_Product_Variation( $variation_id );
                                                                        
                                                                        $regular_price = $variable_product1 ->regular_price;
                                                                        
                                                                        $sales_price = $variable_product1 ->sale_price;
                                                                        
                                                                        echo $barker_currency;
                                                                        echo $regular_price;
                                                                        }
                                                                        ?>	
                                                                    </span><span>Por comida</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                <label class="radio__icon" for="f-three-months">
                                                    <span>
                                                        <svg>
                                                            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#check"></use>
                                                        </svg>
                                                    </span>
                                                </label>
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <?php 
                                                    if($product->product_type=='variable') {
                                                                    
                                                        $available_variations = $product->get_available_variations(); 
                                                    	$count = count($available_variations)-3;
                                                    	$variation_id=$available_variations[$count]['variation_id']; // Getting the variable id of just the 1st product. You can loop $available_variations to get info about each variation.
                                                    	?>                                              	
                                                <input type="radio" name="f-suscription-time" id="f-six-months-<?php echo $barker_icon_type; ?>" value="6 meses" data-variation="<?php echo $variation_id;?>" data-quantity="1">
                                                <label for="f-six-months-<?php echo $barker_icon_type; ?>">
                                                    <div class="radio__content">
                                                        <div class="radio__text">
                                                            <span class="title">Barker 6 meses</span>
                                                            <ul class="price">
                                                                <li><span>
                                                                    <?php 
                                                                        $variable_product1= new WC_Product_Variation( $variation_id );
                                                                        
                                                                        $regular_price = $variable_product1 ->regular_price;
                                                                        
                                                                        $sales_price = $variable_product1 ->sale_price;
                                                                        
                                                                        echo $barker_currency;
                                                                        echo $regular_price;
                                                                        }
                                                                        ?>	
                                                                    </span><span>Por comida</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                <label class="radio__icon" for="f-six-months">
                                                    <span>
                                                        <svg>
                                                            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#check"></use>
                                                        </svg>
                                                    </span>
                                                </label>
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <?php 
                                                    if($product->product_type=='variable') {
                                                                    
                                                        $available_variations = $product->get_available_variations(); 
                                                    	$count = count($available_variations)-3;
                                                    	$variation_id=$available_variations[$count]['variation_id']; // Getting the variable id of just the 1st product. You can loop $available_variations to get info about each variation.
                                                    	?>                                              	
                                                <input type="radio" name="f-suscription-time" id="f-one-year<?php echo $barker_icon_type; ?>" value="1 año" data-variation="<?php echo $variation_id;?>" data-quantity="1">
                                                <label for="f-one-year<?php echo $barker_icon_type; ?>">
                                                    <div class="radio__content">
                                                        <div class="radio__text">
                                                            <span class="title">Barker 1 año</span>
                                                            <ul class="price">
                                                                <li><span>
                                                                    <?php 
                                                                        $variable_product1= new WC_Product_Variation( $variation_id );
                                                                        
                                                                        $regular_price = $variable_product1 ->regular_price;
                                                                        
                                                                        $sales_price = $variable_product1 ->sale_price;
                                                                        
                                                                        echo $barker_currency;
                                                                        echo $regular_price;
                                                                        }
                                                                        ?>	
                                                                    </span><span>Por comida</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                <label class="radio__icon" for="f-one-year">
                                                    <span>
                                                        <svg>
                                                            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#check"></use>
                                                        </svg>
                                                    </span>
                                                </label>
                                                </label>
                                            </div>
                                        </div>
                                        <p class="feedback">* Nota. La entrega del producto es semanal, en la fecha y el horario indicado según la disponibilidad de nuestros distribuidores en cada distrito.</p>
                                    </div>
                                    <!-- Ends PriceBox -->
                                    <?php endforeach; ?>
                                    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                                    <?php endif; ?>
                                    <!-- ////////////////////////////////////////////////// -->
                                    <!-- Ends Price Product-->
                                    <!-- ////////////////////////////////////////////////// -->	
                                </div>
                                <!-- ////////////////////////////////////////////////// -->
                                <!-- Ends Select Product-->
                                <!-- ////////////////////////////////////////////////// -->
                            </div>
                        </div>
                    </section>
                    <!-- ////////////////////////////////////////////////// -->
                    <!-- Step 4-->
                    <!-- ////////////////////////////////////////////////// -->
                    <h3>
                        <span class="icon">
                            <svg>
                                <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#step-delivery"></use>
                            </svg>
                        </span>
                        <span class="text">Pedido</span>
                    </h3>
                    <section>
                        <div class="row justify-content-center">
                            <div class="col-lg-11">
                                <div class="row">
                                    <div class="col-md-6 col-lg-5 order-1">
                                        <div class="delivery-data">
                                            <div class="text-line u-hidden-tablet">
                                                <p>Estás tan cerca de la comida real, ¡Tu perro puede olerla!</p>
                                            </div>
                                            <div class="field-group">
                                                <label>Complete sus datos para<br>comenzar su prueba</label>
                                                <div class="field-wrapper">
                                                    <input type="text" name="f-dog-name" id="f-dog-name" placeholder="Nombre">
                                                </div>
                                                <div class="field-wrapper">
                                                    <input type="text" name="f-dog-name" id="f-dog-name" placeholder="Apellido">
                                                </div>
                                                <div class="field-wrapper">
                                                    <input type="text" name="f-dog-name" id="f-dog-name" placeholder="Número de teléfono">
                                                </div>
                                            </div>
                                            <div class="field-group">
                                                <label>Dirección de envío</label>
                                                <div class="field-wrapper">
                                                    <div class="select">
                                                        <select name="f-shipping-address" id="f-shipping-address">
                                                            <option value="">Distrito</option>
                                                            <option value="Barranco">Barranco</option>
                                                            <option value="Chorrillos">Chorrillos</option>
                                                            <option value="Jesús María">Jesús María</option>
                                                            <option value="La Molina">La Molina</option>
                                                            <option value="Lince">Lince</option>
                                                            <option value="Miraflores">Miraflores</option>
                                                            <option value="San Borja">San Borja</option>
                                                            <option value="San Isidro">San Isidro</option>
                                                            <option value="San Miguel">San Miguel</option>
                                                            <option value="Surco">Surco</option>
                                                            <option value="Surquillo">Surquillo</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="field-wrapper field-wrapper--simple">
                                                    <input class="fill" type="text" name="f-info-date" id="f-info-date" value="">
                                                </div>
                                                <div class="field-wrapper field-wrapper--simple">
                                                    <label>Horario de entrega </label>
                                                    <div class="radio-group radio-group--full row">
                                                        <div class="radio radio--small col-md-6">
                                                            <input type="radio" name="f-delivery-time" id="f-moorning-1" value="cachorro">
                                                            <label class="u-mb--lv0" for="f-moorning-1">
                                                                <div class="radio__content">
                                                                    <div class="radio__text">
                                                                        <h6>9am a 12pm</h6>
                                                                        <p>Turno día</p>
                                                                    </div>
                                                                </div>
                                                            <label class="radio__icon" for="f-moorning-1"></label>
                                                            </label>
                                                        </div>
                                                        <div class="radio radio--small col-md-6">
                                                            <input type="radio" name="f-delivery-time" id="f-afternoon-1" value="adulto">
                                                            <label class="u-mb--lv0" for="f-afternoon-1">
                                                                <div class="radio__content">
                                                                    <div class="radio__text">
                                                                        <h6>1pm a 6pm</h6>
                                                                        <p>Turno tarde</p>
                                                                    </div>
                                                                </div>
                                                            <label class="radio__icon" for="f-afternoon-1"></label>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="field-wrapper">
                                                    <input type="text" name="f-dog-name" id="f-dog-name" placeholder="Av. Jr.">
                                                </div>
                                                <div class="field-wrapper">
                                                    <input type="text" name="f-dog-name" id="f-dog-name" placeholder="Urbanizado">
                                                </div>
                                                <div class="field-wrapper">
                                                    <input type="text" name="f-dog-name" id="f-dog-name" placeholder="Dpto.">
                                                </div>
                                            </div>
                                            <div class="field-group">
                                                <div class="field-wrapper field-wrapper--highlight">
                                                    <div class="radio-group radio-group--square border">
                                                        <div class="radio">
                                                            <input type="radio" name="f-housing-type" id="f-house" value="Casa">
                                                            <label for="f-house">Casa</label>
                                                        </div>
                                                        <div class="radio">
                                                            <input type="radio" name="f-housing-type" id="f-fifth" value="Quinta">
                                                            <label for="f-fifth">Quinta</label>
                                                        </div>
                                                        <div class="radio">
                                                            <input type="radio" name="f-housing-type" id="f-building" value="Edificio">
                                                            <label for="f-building">Edificio</label>
                                                        </div>
                                                        <div class="radio">
                                                            <input type="radio" name="f-housing-type" id="f-condominium" value="Condominio">
                                                            <label for="f-condominium">Condominio</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="field-group">
                                                <div class="field-wrapper">
                                                    <label>Datos de facturación</label>
                                                    <figure><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tarjetas.png" alt=""></figure>
                                                </div>
                                                <div class="field-wrapper field-wrapper--simple">
                                                    <input type="text" name="f-cardholder-name" id="f-cardholder-name" placeholder="Nombre del titular de la tarjeta">
                                                </div>
                                                <div class="field-wrapper field-wrapper--simple">
                                                    <input type="text" name="f-cardholder-dni" id="f-cardholder-dni" placeholder="DNI del titular de la tarjeta">
                                                </div>
                                                <div class="field-wrapper field-wrapper--simple">
                                                    <label for="f-card-number">Número de tarjeta</label>
                                                    <input type="text" name="f-card-number" id="f-card-number" placeholder="Ingrese los 14 dígitos de la tarjeta">
                                                </div>
                                                <div class="field-wrapper field-wrapper--simple">
                                                    <label>Fecha de vencimiento</label>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="select">
                                                                <select name="f-expiration-month-card" id="f-expiration-month-card">
                                                                    <option value="">Mes</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="select">
                                                                <select name="f-expiration-year-card" id="f-expiration-year-card">
                                                                    <option value="">Año</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="field-wrapper field-wrapper--simple">
                                                    <label>Código de seguridad</label>
                                                    <input type="text" name="f-code-security" id="f-code-security" placeholder="CVC">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6 offset-lg-1 order-md-1">
                                        <div class="order-detail">
                                            <div class="order-detail__head u-text-center">
                                                <div class="subtitle-xs">
                                                    <h4>Resumen del pedido</h4>
                                                </div>
                                            </div>
                                            <div class="order-detail__body">
                                                <div class="order-list">
                                                    <div class="order-item">
                                                        <div class="order-item__head u-text-center">
                                                            <h4>Caja de <span class="js-dog-name">tu mascota</span></h4>
                                                        </div>
                                                        <div class="order-item__body">
                                                            <div class="order-item__text">
                                                                <div class="order-item__text-desc">
                                                                    <h5>Barker por una semana</h5>
                                                                    <p>7 comidas de pollo</p>
                                                                    <p>Cargando y entregando semanal</p>
                                                                    <p>Todas en presentación de 1KG, contenidas en 20 hamburguesas de 50 gramos cada una.</p>
                                                                </div>
                                                                <div class="order-item__text-amount">S/. <span>90.00</span></div>
                                                            </div>
                                                            <div class="order-item__action"><span class="link-action js-edit-order">Editar</span><span class="link-action js-remove-order">Eliminar</span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="order-action">
                                                    <p class="u-text-center"><span class="link-action js-add-dog">+ Agregar otra mascota</span></p>
                                                    <div class="modal-inner dog-new-modal" id="add-dog-new">
                                                        <div class="dog-new-modal__wrap form">
                                                            <div class="field-wrapper field-wrapper--highlight name-dog">
                                                                <label for="f-dog-name-new">¿Cómo se llama tu perro?</label>
                                                                <input type="text" name="f-dog-name-new" id="f-dog-name-new" placeholder="Ejemplo: Boby" autocomplete="off">
                                                            </div>
                                                            <div class="dog-new-modal__action">
                                                                <p>¿<span class="js-dog-name-2">Tu mascota</span> cuenta con las mismas características?</p>
                                                                <div class="dog-new-modal__action-buttons js-confirm-similar-info">
                                                                    <button class="btn btn--secondary" href="" data-similar-info="not">No</button>
                                                                    <button class="btn btn--primary" href="" data-similar-info="yes">Si</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="order-detail__footer">
                                                <div class="order-detail__row">
                                                    <dl>
                                                        <dt>Fecha de entrega estimada</dt>
                                                        <dd>Mié, 04/25</dd>
                                                    </dl>
                                                </div>
                                                <div class="order-detail__row">
                                                    <dl>
                                                        <dt>Descuento de<br>primera orden</dt>
                                                        <dd><small class="old">S/. 180.00</small> <span>S/. 175.00</span></dd>
                                                        <dt><small>Envío</small></dt>
                                                        <dd><small>¡GRATIS!</small></dd>
                                                        <dt>Primer pedido<br>total</dt>
                                                        <dd>S/. 175.00</dd>
                                                    </dl>
                                                </div>
                                                <div class="order-detail__row u-not-border u-text-center">
                                                    <p class="feedback">Puedes cancelar la suscripción en<br>cualquier momento.</p>
                                                </div>
                                                <div class="order-detail__row u-not-border u-flex justify-content-center">
                                                    <div class="checkbox-terms u-flex">
                                                        <input type="checkbox" name="terms" id="terms">
                                                        <label class="icon" for="terms"></label>
                                                        <label for="terms">Estoy de acuerdo con los <a href="" target="_blank">términos y condiciones</a></label>
                                                    </div>
                                                </div>
                                                <div class="order-detail__row u-not-border">
                                                    <div class="coupon fields">
                                                        <input type="text" name="discount-coupon" placeholder="Ingresar cupón de descuento">
                                                        <button class="btn btn--secondary btn--block is-disabled" type="button">Validar cupón</button>
                                                    </div>
                                                </div>
                                                <div class="order-detail__row u-not-border u-hidden-tablet">
                                                    <div class="fields">
                                                        <button class="btn btn--primary btn--block" type="submit">Realizar pedido</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </form>
        </div>
    </main>
</div>
<?php endif ?>
<?php
get_footer();