<?php
get_header();
?>
<nav class="nav-secondary">
      <div class="nav-secondary__content">
         <div class="container u-flex"><a href="<?php echo home_url().'/nosotros' ?>">Nosotros</a><a href="<?php echo home_url().'/ingredientes' ?>">Ingredientes</a><a class="is-active" href="<?php echo home_url().'/como-funciona' ?>">Cómo funciona</a><a href="<?php echo home_url().'/faq' ?>">Preguntas frecuentes</a></div>
      </div>
    </nav>
    <div class="page-wrap">
      <main class="main">
        <div class="page-name">
          <h2>Acerca de Barker</h2>
        </div>
        <section class="section what-is">
          <div class="container">
            <!-- Que es Barker-->
            <div class="row align-items-center u-text-justify">
              <div class="title col u-visible-tablet-wide">
                <h2>¿Qué es Barker?</h2>
              </div>
              <div class="col-lg-7 order-1 what-is__text">
                <div class="title u-hidden-tablet-wide">
                  <h2><?php echo get_theme_mod('bk_barker'); ?></h2>
                </div>
                <p><?php echo get_theme_mod('bk_contenido'); ?></p>
              </div>
              <div class="what-is__img col-lg-5 order-lg-1" id="scene">
                <div class="what-is__food food">
                  <div class="food__item food__item--insumos"></div>
                  <div class="food__item food__item--preparada js-slide-toggle"></div>
                </div>
              </div>
            </div>
            <!-- /Ends Que es Barker-->
          </div>
        </section>
        <section class="section aboutus">
          <div class="container">
            <div class="row justify-content-center u-text-center">
              <div class="col-lg-10">
                <div class="title u-text-center">
                  <h3>Nosotros</h3>
                </div>
                <div class="team tab">
                  <?php
                $resPosts=query_posts(array('post_type' => 'nosotros-barker','posts_per_page' => 0) );
                  if(count($resPosts)>0){
                      $i=1;
                      foreach ($resPosts as $resPost) { 
                          $imagenesNosotros.='<li class="members__item" data-tab="'.$i.'">
                      <figure class="members__item-img">
                        <img src="'.wp_get_attachment_image_src( get_post_thumbnail_id( $resPost->ID),'categoria-default')[0].'">
                        <img src="'.wp_get_attachment_image_src( get_post_thumbnail_id( $resPost->ID),'categoria-default')[0].'" alt=""></figure>
                      <div class="members__item-text">
                        <h6>'.$resPost->post_title.'</h6>
                      </div>
                    </li>';
                    $contenidoNosotros.='<div class="tab__content-item" id="'.$i.'"><p>'.$resPost->post_excerpt.'</p></div>';
                          $i++;
                      }
                  }
                  wp_reset_query();
              ?>
                  <div class="team-members js-tabs hover">
                    <ul class="members tab__items">
                      <?php echo $imagenesNosotros; ?>
                    </ul>
                  </div>
                  <div class="tab__content">
                   <?php echo $contenidoNosotros; ?>
                  </div>
                </div>
                <p class="u-text-center"><a class="btn btn--primary" href="<?php echo home_url().'/pedidos' ?>">Diseña tu pedido</a></p>
              </div>
            </div>
          </div>
        </section>
      </main>
    </div>
     <?php
get_footer();