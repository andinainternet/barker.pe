<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="shortcut icon" type="image/png" href="">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <title><?php wp_title(); ?></title>
    <?php wp_head(); ?>
    <script type="text/javascript">
    var baseAssets="<?php echo get_template_directory_uri(); ?>/assets/";
    </script>
  </head>
<body <?php body_class(); ?>> <!-- https://developer.wordpress.org/reference/functions/body_class/ -->
     <nav class="site-nav js-site-nav">
      <div class="container-fluid">
        <div class="u-flex justify-content-between align-items-center">
          <div class="site-nav__logo"><a class="u-flex" href="<?php echo home_url() ?>">
              <svg>
                <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#barker"></use>
              </svg></a></div>
          <div class="site-nav__main u-flex">
            <div class="site-nav__links">
              <div class="site-nav__close-menu u-visible-tablet"></div>
              <div class="site-nav__links-wrap">
                <ul>
                      <li><a class="site-nav__item js-btn-barker-nav" href="<?php echo home_url().'/nosotros' ?>"><span class="text">Conócenos</span></a>
                        <div class="barker-menu">
                          <div class="barker-menu__header">
                            <button class="js-btn-barker-nav"><span class="icon">
                                <svg>
                                  <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#back"></use>
                                </svg></span><span class="text">Volver</span></button>
                          </div>
                          <div class="barker-menu__content">
                            <ul class="barker-menu__list">
                              <li><a class="site-nav__item" href="<?php echo home_url().'/nosotros' ?>">Nosotros</a></li>
                              <li><a class="site-nav__item" href="<?php echo home_url().'/ingredientes' ?>">Ingredientes</a></li>
                              <li><a class="site-nav__item" href="<?php echo home_url().'/como-funciona' ?>">Como funciona</a></li>
                              <li><a class="site-nav__item" href="<?php echo home_url().'/faq' ?>">Preguntas frecuentes</a></li>
                            </ul>
                          </div>
                        </div>
                      </li>
                      <li><a class="site-nav__item" href="<?php echo home_url().'/sabores' ?>"><span class="text">Sabores</span></a>
                      </li>
                      <li><a class="site-nav__item" href="<?php echo home_url().'/punto-de-venta' ?>"><span class="text">Puntos de venta</span></a>
                      </li>
                      <li><a class="site-nav__item" href="<?php echo home_url().'/faq' ?>"><span class="text">Preguntas frecuentes</span></a>
                      </li>
                      <li><a class="site-nav__item" href="<?php echo home_url().'/suscripcion' ?>"><span class="text">Suscripción</span></a>
                      </li>
                      <li><a class="site-nav__item" href="<?php echo home_url().'/blog' ?>"><span class="text">Blog</span></a>
                      </li>
                </ul>
                <div class="site-nav__aditional u-visible-tablet">
                  <ul class="site-nav__aditional-data">
                    <li><span>
                        <svg>
                          <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#email"></use>
                        </svg></span>contacto@barker.pe</li>
                    <li><span>
                        <svg>
                          <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#phone"></use>
                        </svg></span>991750566</li>
                    <li><span>
                        <svg>
                          <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#pin"></use>
                        </svg></span>Gerona 225 - San Borja</li>
                  </ul>
                  <ul class="site-nav__aditional-social">
                    <li><a href="">
                        <svg>
                          <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#whatsapp"></use>
                        </svg></a></li>
                    <li><a href="">
                        <svg>
                          <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#facebook"></use>
                        </svg></a></li>
                    <li><a href="">
                        <svg>
                          <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#instagram"></use>
                        </svg></a></li>
                  </ul>
                  <ul class="site-nav__aditional-plus">
                    <li><a href="">Términos</a></li>
                    <li><a href="">Privacidad</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="site-nav__actions u-flex"><a class="site-nav__item" href="<?php echo home_url().'/pedidos' ?>"><span class="icon">
                  <svg>
                    <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#pedido"></use>
                  </svg></span><span class="text u-hidden-desktop">Pedidos</span></a><a class="site-nav__item" href="<?php echo home_url().'/cuenta' ?>"><span class="icon u-visible-tablet">
                  <svg>
                    <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#user"></use>
                  </svg></span><span class="text">Iniciar sesión</span></a>
              <button class="site-nav__item navicon js-btn-menu u-visible-tablet"><span class="icon"></span></button>
            </div>
          </div>
        </div>
      </div>
    </nav>