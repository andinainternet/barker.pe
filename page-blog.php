<?php
get_header();
$cat = get_terms('category',
                      array('hide_empty' => false,'parent'=>0,'order'=> 'ASC'));
?>
   <div class="page-wrap">
      <main class="main blog">
        <div class="blog-banner">
          <div class="owl-carousel js-blog-carousel">
            <?php
                $resPosts=get_posts(array('post_type' => 'slider-blog','posts_per_page' => 3) );
                foreach ($resPosts as $resPost) { 
                  echo '
                  <div class="blog-banner__item" style="background-image: url(\''.wp_get_attachment_image_src( get_post_thumbnail_id( $resPost->ID),'full')[0].'\');">
                    <div class="container u-full-height u-flex align-items-end">
                 <div class="blog-banner__text">
                 <h2>'.$resPost->post_title.'</h2>
                  <p>'.$resPost->post_excerpt.'</p>
                </div>
              </div>
            </div> ';
            }
              ?>
          
          </div>
        </div>
        <div class="blog-categories">
          <div class="container">
            <div class="blog-categories__carousel">
              <div class="owl-carousel js-categories-blog-carousel">
                <?php
                    foreach($cat as $row){
                      echo '
                <div class="blog-categories__item"><a href="'.get_category_link($row->term_id).'"class="item cat-clss-'.$row->slug.'">'.$row->name.'</a></div>';
                 }
                  ?>
              </div>
            </div>
          </div>
        </div>
        <section class="section">
          <div class="container">
            <div class="row">
             <?php
                      $resPosts=query_posts('posts_per_page=6&order=DESC');
                      if(count($resPosts)>0){
                        foreach ($resPosts as $resPost) {
                          echo '
              <div class="col-sm-6 col-lg-4 card-news"><a class="card-news__wrapper" href="'.get_permalink($resPost->ID).'">
                  <figure class="card-news__image"><img src="'.wp_get_attachment_image_src( get_post_thumbnail_id( $resPost->ID),'categoria-default')[0].'" alt=""></figure>
                  <div class="card-news__content">
                    <h2 class="card-news__title">'.$resPost->post_title.'</h2><span class="card-news__date"></span>
                    <p>'.substr($resPost->post_content,0,150).'</p>
                  </div></a></div>';
                        }
                      }
                      wp_reset_query();
                    ?>
            </div>
          </div>
        </section>
      </main>
    </div>
    <?php
get_footer();
