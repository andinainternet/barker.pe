
 	
PHP Beautifier
Beautify and format your PHP code
 
Here is your PHP code :
 	
 Source
 
See as Text 	

<?php
date_default_timezone_set('America/Lima');

// include_once('includes/acf.php');

function incluir_scripts()
{
	wp_enqueue_style('style', get_stylesheet_directory_uri() . '/style.css');
	wp_enqueue_style('main', get_stylesheet_directory_uri() . '/assets/css/main.min.css');
	wp_enqueue_style('barker-css', get_stylesheet_directory_uri() . '/assets/css/barker-css.css');
	wp_enqueue_script("jquery");
	wp_enqueue_script('pagina', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.14.0/TweenMax.min.js', '', '', true);
	wp_enqueue_script('web', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/1.3.0/jquery.scrollmagic.min.js', '', '', true);
	wp_enqueue_script('enlace', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/1.3.0/jquery.scrollmagic.debug.js', '', '', true);
	wp_enqueue_script('main', get_stylesheet_directory_uri() . '/assets/js/main.min.js', '', '', true);
	wp_enqueue_script('barker-js', get_stylesheet_directory_uri() . '/assets/js/barker-js.js', '', '', true);
}

add_action('wp_enqueue_scripts', 'incluir_scripts', 1);

function modify_jquery()
{
	if (!is_admin()) {

		// comment out the next two lines to load the local copy of jQuery

		wp_deregister_script('jquery');
		wp_register_script('jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js', '', '', true);
		wp_enqueue_script('jquery');
	}
}

add_action('init', 'modify_jquery');
wp_register_theme_activation_hook('barker', 'wpse_25885_theme_activate');
wp_register_theme_deactivation_hook('barker', 'wpse_25885_theme_deactivate');
/**
 *
 * @desc registers a theme activation hook
 * @param string $code : Code of the theme. This can be the base folder of your theme. Eg if your theme is in folder 'mytheme' then code will be 'mytheme'
 * @param callback $function : Function to call when theme gets activated.
 */

function wp_register_theme_activation_hook($code, $function)
{
	$optionKey = "theme_is_activated_" . $code;
	if (!get_option($optionKey)) {
		call_user_func($function);
		update_option($optionKey, 1);
	}
}

/**
 * @desc registers deactivation hook
 * @param string $code : Code of the theme. This must match the value you provided in wp_register_theme_activation_hook function as $code
 * @param callback $function : Function to call when theme gets deactivated.
 */

function wp_register_theme_deactivation_hook($code, $function)
{

	// store function in code specific global

	$GLOBALS["wp_register_theme_deactivation_hook_function" . $code] = $function;

	// create a runtime function which will delete the option set while activation of this theme and will call deactivation function provided in $function

	$fn = create_function('$theme', ' call_user_func($GLOBALS["wp_register_theme_deactivation_hook_function' . $code . '"]); delete_option("theme_is_activated_' . $code . '");');

	// add above created function to switch_theme action hook. This hook gets called when admin changes the theme.
	// Due to wordpress core implementation this hook can only be received by currently active theme (which is going to be deactivated as admin has chosen another one.
	// Your theme can perceive this hook as a deactivation hook.)

	add_action("switch_theme", $fn);
}

function wpse_25885_theme_activate()
{
	$default_pages = array(
		array(
			'title' => 'Nosotros',
			'content' => ''
		) ,
		array(
			'title' => 'Punto de venta',
			'content' => ''
		) ,
		array(
			'title' => 'Sabores',
			'content' => ''
		) ,
		array(
			'title' => 'Blog',
			'content' => ''
		) ,
		array(
			'title' => 'Cuenta',
			'content' => ''
		) ,
		array(
			'title' => 'Faq',
			'content' => ''
		) ,
		array(
			'title' => 'Suscripción',
			'content' => ''
		)
	);
	$existing_pages = get_pages();
	$existing_titles = array();
	foreach($existing_pages as $page) {
		$existing_titles[] = $page->post_title;
	}

	foreach($default_pages as $new_page) {
		if (!in_array($new_page['title'], $existing_titles)) {

			// create post object

			$add_default_pages = array(
				'post_title' => $new_page['title'],
				'post_content' => $new_page['content'],
				'post_status' => 'publish',
				'post_type' => 'page'
			);

			// insert the post into the database

			$result = wp_insert_post($add_default_pages);
		}
	}
}

function wpse_25885_theme_deactivate()
{

	// code to execute on theme deactivation

}

function mytheme_customize_register($wp_customize)
{

	// lo que va incluido en el panel

	$wp_customize->add_panel('bk_panel', array(
		'title' => __('Personalizar Barker') ,
		'description' => $description, // Include html tags such as <p>.
		'priority' => 160

		// Mixed with top-level-section hierarchy.

	));
	$wp_customize->add_section("bk_administrar_inicio", array(
		'title' => "Administrar Inicio",
		'panel' => 'bk_panel'
	));
	$wp_customize->add_setting('bk_titulo', array(
		'default' => ''

		// LO QUE TU MASCOTA ELEGIRÍA

	));
	$wp_customize->add_control('bk_titulo', array(
		'label' => 'Titulo',
		'section' => 'bk_administrar_inicio',
		'type' => 'text',
		'settings' => 'bk_titulo'
	));
	$wp_customize->add_setting('bk_subtitulo', array(
		'default' => ''

		// Barker significa disfrutar de la mejor versión de nuestras mascotas.

	));
	$wp_customize->add_control('bk_subtitulo', array(
		'label' => 'Subtitulo',
		'section' => 'bk_administrar_inicio',
		'type' => 'text',
		'settings' => 'bk_subtitulo'
	));
	/* $wp_customize->add_setting('bk_imagen', array(
	'default'        => '',
	));
	$wp_customize->add_control('bk_imagen', array(
	'label'   => 'Imagen de Fondo',
	'section' => 'bk_administrar_inicio',
	'type'    => 'text',
	'settings'=>'bk_imagen',
	));*/
	$wp_customize->add_section("bk_administrar_contenido", array(
		'title' => "Administrar Contenido",
		'panel' => 'bk_panel'
	));
	$wp_customize->add_setting('bk_barker', array(
		'default' => ''

		// ¿Que es Barker?

	));
	$wp_customize->add_control('bk_barker', array(
		'label' => 'Que es Barker',
		'section' => 'bk_administrar_contenido',
		'type' => 'text',
		'settings' => 'bk_barker'
	));
	$wp_customize->add_setting('bk_contenido', array(
		'default' => ''

		// Barker significa disfrutar de la mejor versión de nuestras mascotas. Hoy, somos conscientes que lo que comemos se relaciona directamente con nuestra calidad de vida. Lo mismo pasa con nuestras mascotas. Barker nace de la tendencia BARF (Biologically Appropiate Raw Food) proponiendo una formula balanceada y responsable que se adapta al estilo de vida de un perro hoy en día. Nuestra comida está basada en proteína cruda, vegetales y vitaminas.

	));
	$wp_customize->add_control('bk_contenido', array(
		'label' => 'Contenido del Tituto',
		'section' => 'bk_administrar_contenido',
		'type' => 'text',
		'settings' => 'bk_contenido'
	));
	$wp_customize->add_section("bk_redes", array(
		'title' => "Personalizar Redes",
		'panel' => 'bk_panel'
	));
	$wp_customize->add_setting('bk_facebook', array(
		'default' => ''
	));
	$wp_customize->add_control('bk_facebook', array(
		'label' => 'Facebook',
		'section' => 'bk_redes',
		'type' => 'text',
		'settings' => 'bk_facebook'
	));
	$wp_customize->add_setting('bk_mail', array(
		'default' => ''
	));
	$wp_customize->add_control('bk_mail', array(
		'label' => 'Mail',
		'section' => 'bk_redes',
		'type' => 'text',
		'settings' => 'bk_mail'
	));
	$wp_customize->add_setting('bk_whatsapp', array(
		'default' => ''
	));
	$wp_customize->add_control('bk_whatsapp', array(
		'label' => 'Whatsapp',
		'section' => 'bk_redes',
		'type' => 'text',
		'settings' => 'bk_whatsapp'
	));
	$wp_customize->add_setting('bk_instagram', array(
		'default' => ''
	));
	$wp_customize->add_control('bk_instagram', array(
		'label' => 'Instagram',
		'section' => 'bk_redes',
		'type' => 'text',
		'settings' => 'bk_instagram'
	));
}

add_action('customize_register', 'mytheme_customize_register');

function create_posttype()
{
	register_post_type('nosotros-barker', array(
		'labels' => array(
			'name' => __('Nosotros') ,
			'singular_name' => __('Nosotros') ,
			'add_new' => __('Agregar Nosotros') ,
			'add_new_item' => __('Nosotros') ,
			'edit_item' => __('Nosotros')
		) ,
		'public' => true,
		'has_archive' => true,

		// 'rewrite' => array('slug' => '/'),
		// 'taxonomies'          => array( 'post_tag','category' ),

		'supports' => array(
			'title',
			'excerpt',
			'thumbnail'
		)
	));
	register_post_type('beneficios', array(
		'labels' => array(
			'name' => __('Beneficios') ,
			'singular_name' => __('Beneficios') ,
			'add_new' => __('Agregar Beneficios') ,
			'add_new_item' => __('Beneficios') ,
			'edit_item' => __('Beneficios')
		) ,
		'public' => true,
		'has_archive' => true,

		// 'rewrite' => array('slug' => '/'),
		// 'taxonomies'          => array( 'post_tag','category' ),

		'supports' => array(
			'title',
			'excerpt',
			'thumbnail'
		)
	));
	register_post_type('pasos', array(
		'labels' => array(
			'name' => __('Pasos') ,
			'singular_name' => __('Pasos') ,
			'add_new' => __('Agregar Pasos') ,
			'add_new_item' => __('Pasos') ,
			'edit_item' => __('Pasos')
		) ,
		'public' => true,
		'has_archive' => true,

		// 'rewrite' => array('slug' => '/'),
		// 'taxonomies'          => array( 'post_tag','category' ),

		'supports' => array(
			'title',
			'excerpt',
			'thumbnail'
		)
	));
	register_post_type('testimonios', array(
		'labels' => array(
			'name' => __('Testimonios') ,
			'singular_name' => __('Testimonios') ,
			'add_new' => __('Agregar Testimonios ') ,
			'add_new_item' => __('Testimonios') ,
			'edit_item' => __('Testimonios')
		) ,
		'public' => true,
		'has_archive' => true,

		// 'rewrite' => array('slug' => '/'),
		// 'taxonomies'          => array( 'post_tag','category' ),

		'supports' => array(
			'title',
			'excerpt',
			'thumbnail'
		)
	));
	register_post_type('preguntas-frecuentes', array(
		'labels' => array(
			'name' => __('Preguntas Frecuentes') ,
			'singular_name' => __('Preguntas Frecuentes') ,
			'add_new' => __('Agregar Preguntas ') ,
			'add_new_item' => __('Preguntas Frecuentes') ,
			'edit_item' => __('Preguntas Frecuentes')
		) ,
		'public' => true,
		'has_archive' => true,
		'rewrite' => array(
			'slug' => 'preguntas-frecuentes/%categorias-preguntas%'
		) ,
		'taxonomies' => array(
			'categorias-preguntas'
		) ,
		'supports' => array(
			'title',
			'excerpt',
			'editor'
		)
	));
	register_post_type('punto-de-ventas', array(
		'labels' => array(
			'name' => __('Puntos de Ventas') ,
			'singular_name' => __('Puntos de ventas') ,
			'add_new' => __('Agregar Puntos de Ventas ') ,
			'add_new_item' => __('Puntos de Ventas') ,
			'edit_item' => __('Puntos de Ventas')
		) ,
		'public' => true,
		'has_archive' => true,

		// 'rewrite' => array('slug' => '/'),
		// 'taxonomies'          => array( 'post_tag','category' ),

		'supports' => array(
			'title'
		)
	));
	register_post_type('slider-blog', array(
		'labels' => array(
			'name' => __('Slider Blog') ,
			'singular_name' => __('Slider Bog') ,
			'add_new' => __('Agregar Slider BLog ') ,
			'add_new_item' => __('Slider Blog') ,
			'edit_item' => __('Slider Blog')
		) ,
		'public' => true,
		'has_archive' => true,

		// 'rewrite' => array('slug' => '/'),
		// 'taxonomies'          => array( 'post_tag','category' ),

		'supports' => array(
			'title',
			'excerpt',
			'thumbnail'
		)
	));
	register_post_type('slider-home', array(
		'labels' => array(
			'name' => __('Slider Home') ,
			'singular_name' => __('Slider Home') ,
			'add_new' => __('Agregar Slider Home ') ,
			'add_new_item' => __('Slider Home') ,
			'edit_item' => __('Slider Home')
		) ,
		'public' => true,
		'has_archive' => true,

		// 'rewrite' => array('slug' => '/'),
		// 'taxonomies'          => array( 'post_tag','category' ),

		'supports' => array(
			'title',
			'thumbnail'
		)
	));
}

// Hooking up our function to theme setup

add_action('init', 'create_posttype');

// Lo enganchamos en la acción init y llamamos a la función create_book_taxonomies() cuando arranque

add_action('init', 'create_book_taxonomies', 0);

// Creamos dos taxonomías, género y autor para el custom post type "libro"

function create_book_taxonomies()
{

	// Añadimos nueva taxonomía y la hacemos jerárquica (como las categorías por defecto)

	$labels = array(
		'name' => _x('Categorías', 'taxonomy general name')
	);
	register_taxonomy('categorias-preguntas', array(
		'categorias-preguntas'
	) , array(
		'hierarchical' => true,
		'labels' => $labels, // ADVERTENCIA: Aquí es donde se utiliza la variable $labels
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array(
			'slug' => 'productos'
		)
	));
}

add_theme_support('post-thumbnails');
add_action('user_register', 'crf_user_register');

function crf_user_register($user_id)
{
	if (!empty($_POST['rg-phone'])) {
		update_user_meta($user_id, 'rg-phone', esc_sql($_POST['rg-phone']));
	}
}

function redirect_login_page()
{
	$register_page = home_url('/cuenta');
	$login_page = home_url('/cuenta?action=login');
	$page_viewed = basename($_SERVER['REQUEST_URI']);
	if ($page_viewed == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET') {
		wp_redirect($login_page);
		exit;
	}

	if ($page_viewed == "wp-login.php?action=register" && $_SERVER['REQUEST_METHOD'] == 'GET') {
		wp_redirect($register_page);
		exit;
	}
}

add_action('init', 'redirect_login_page');

// Redirect For Login Failed

function login_failed()
{
	wp_redirect(home_url('/cuenta?action=login&login=failed'));
	exit;
}

add_action('wp_login_failed', 'login_failed');

// Redirect For Empty Username Or Password

function verify_username_password($user, $username, $password)
{
	if ($username == "" || $password == "") {
		wp_redirect(home_url('/cuenta?action=login&login=empty'));
		exit;
	}
}

add_filter('authenticate', 'verify_username_password', 1, 3);
add_filter('show_admin_bar', '__return_false');

// ===============================================
// ----------------------------------------------|
// ANDINA SOFTWARE-> INIT BARKER CUSTOM CHECKOUT +
// ----------------------------------------------|
// ===============================================



// DIRECT CHECKOUT FOR WOOCOMMERCE
// -----------------------------------------------

function andina_software_redirect_checkout_add_cart($url) {
	
	$url = get_permalink(get_option('woocommerce_checkout_page_id'));
	return $url;
}

add_filter('woocommerce_add_to_cart_redirect', 'andina_software_redirect_checkout_add_cart');


// ANDINA SOFTWARE > CUSTOM FIELD FOR SUBSCRIPTION
// -----------------------------------------------

if (function_exists("register_field_group")) {
	
	register_field_group(array(
		'id' => 'acf_suscripcion',
		'title' => 'Suscripción',
		'fields' => array(
			array(
				'key' => 'field_5b69fa5f153b8',
				'label' => 'Productos',
				'name' => 'barker_suscripcion_productos',
				'type' => 'relationship',
				'post_type' => array(
					0 => 'product'
				) ,
				'taxonomy' => array(
					0 => 'all'
				) ,
				'filters' => array(
					0 => 'search',
					1 => 'post_type'
				) ,
				'result_elements' => array(
					0 => 'featured_image',
					1 => 'post_title',
					2 => 'post_type'
				) ,
				'max' => '',
				'return_format' => 'object'
			)
		) ,
		'location' => array(
			array(
				array(
					'param' => 'page',
					'operator' => '==',
					'value' => '125',
					'order_no' => 0,
					'group_no' => 0
				)
			)
		) ,
		'options' => array(
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array()
		) ,
		'menu_order' => 2
	));
}

// REMOVE GENERALES FIELDS WOOCOMMERCE CHECKOUT
// -----------------------------------------------

add_filter('woocommerce_checkout_fields', 'andina_software_remover_campos_checkout');

function andina_software_remover_campos_checkout($fields) {

	unset($fields['order']['order_comments']); // Comentarios
	unset($fields['billing']['billing_company']); // Compañia
	unset($fields['billing']['billing_postcode']); // CEP

	return $fields;
}


// ORDER GENERALES FIELDS WOOCOMMERCE BILLING
// -----------------------------------------------

add_filter('woocommerce_checkout_fields', 'woocommerce_billing_fields_custom', 9999);

function woocommerce_billing_fields_custom($fields)
{
	$fields['billing']['billing_first_name']['priority'] = 10;
	$fields['billing']['billing_first_name']['label'] = '';
	$fields['billing']['billing_first_name']['placeholder'] = 'Nombre';
	$fields['billing']['billing_first_name']['class'] = array(
		'form-row-wide'
	);
	$fields['billing']['billing_last_name']['priority'] = 20;
	$fields['billing']['billing_last_name']['label'] = '';
	$fields['billing']['billing_last_name']['placeholder'] = 'Apellido';
	$fields['billing']['billing_last_name']['class'] = array(
		'form-row-wide '
	);
	$fields['billing']['billing_phone']['priority'] = 30;
	$fields['billing']['billing_phone']['label'] = '';
	$fields['billing']['billing_phone']['placeholder'] = 'Número de teléfono';
	$fields['billing']['billing_phone']['class'] = array(
		'form-row-wide'
	);
	$fields['billing']['billing_country']['class'] = array(
		'barker-checkout_hidenField'
	);
	$fields['billing']['billing_country']['priority'] = 80;
	$fields['billing']['billing_email']['class'] = array(
		'barker-checkout_hidenField'
	);
	$fields['billing']['billing_email']['priority'] = 90;
	return $fields;
}

add_filter('woocommerce_default_address_fields', 'custom_override_default_locale_fields');

function custom_override_default_locale_fields($fields)
{
	$fields['state']['priority'] = 40;
	$fields['state']['label'] = 'Dirección de envío';
	$fields['state']['class'] = array(
		'form-row-wide'
	);
	$fields['state']['class'] = array(
		'barker-checkout_hidenField'
	);
	$fields['address_1']['priority'] = 50;
	$fields['address_1']['label'] = '';
	$fields['address_1']['placeholder'] = 'Av. Jr.';
	$fields['address_1']['class'] = array(
		'form-row-wide'
	);
	$fields['address_2']['priority'] = 60;
	$fields['address_2']['placeholder'] = 'Urbanizado';
	$fields['address_2']['class'] = array(
		'form-row-wide'
	);
	$fields['city']['priority'] = 70;
	$fields['city']['label'] = '';
	$fields['city']['placeholder'] = 'Dpto';
	$fields['city']['class'] = array(
		'form-row-wide'
	);
	return $fields;
}

// ADD & CONVERT CUSTOM INPUT RADIO BILLING FIELD --> DELIVERY TIME
// -----------------------------------------------------------------

function woocommerce_form_field_radio($key, $args, $value = '')
{
	global $woocommerce;
	$defaults = array(
		'type' => 'radio',
		'label' => '',
		'placeholder' => '',
		'required' => false,
		'class' => array() ,
		'label_class' => array() ,
		'return' => false,
		'options' => array()
	);
	$args = wp_parse_args($args, $defaults);
	if ((isset($args['clear']) && $args['clear'])) $after = '
				<div class="clear"></div>
				';
	else $after = '';
	$required = ($args['required']) ? ' <abbr class="required" title="' . esc_attr__('required', 'woocommerce') . '">*</abbr>' : '';
	switch ($args['type']) {
	case "select":
		$options = '';
		if (!empty($args['options']))
		foreach($args['options'] as $option_key => $option_text) $options.= '<div class="barker-checkout-radio-container"><label for="' . $key . '" class="' . implode(' ', $args['label_class']) . '">' . $args['label'] . $required . $option_text . '</label><input type="radio" name="' . $key . '" value="' . $option_key . '" ' . selected($value, $option_key, false) . 'class="select"></div>' . "\r\n";
		$field = '
					<p class="form-row ' . implode(' ', $args['class']) . '" id="' . $key . '_field">
						
						' . $options . '
					</p>
						
						' . $after;
		break;
	} //$args[ 'type' ]
	if ($args['return']) return $field;
	else echo $field;
}

// ADD & CONVERT CUSTOM INPUT RADIO BILLING FIELD --> KIND HOUSE
// -----------------------------------------------------------------

function woocommerce_form_field_radio_house($key, $args, $value = '')
{
	global $woocommerce;
	$defaults = array(
		'type' => 'radio',
		'label' => '',
		'placeholder' => '',
		'required' => false,
		'class' => array() ,
		'label_class' => array() ,
		'return' => false,
		'options' => array()
	);
	$args = wp_parse_args($args, $defaults);
	if ((isset($args['clear']) && $args['clear'])) $after = '
				<div class="clear"></div>
				';
	else $after = '';
	$required = ($args['required']) ? ' <abbr class="required" title="' . esc_attr__('required', 'woocommerce') . '">*</abbr>' : '';
	switch ($args['type']) {
	case "select":
		$options = '';
		if (!empty($args['options']))
		foreach($args['options'] as $option_key => $option_text) $options.= '<div class="barker-checkout-radio-house-container"><label for="' . $key . '" class="' . implode(' ', $args['label_class']) . '">' . $args['label'] . $required . $option_text . '</label><input type="radio" name="' . $key . '" value="' . $option_key . '" ' . selected($value, $option_key, false) . 'class="select"></div>' . "\r\n";
		$field = '
					<p class="form-row ' . implode(' ', $args['class']) . '" id="' . $key . '_field">
						
						' . $options . '
					</p>
						
						' . $after;
		break;
	} //$args[ 'type' ]
	if ($args['return']) return $field;
	else echo $field;
}



// ADD CUSTOM INPUT RADIO BILLING FIELD ON CHECKOUT --> DELIVERY TIME
// -----------------------------------------------------------------

add_action('woocommerce_after_checkout_billing_form', 'barker_horario_reparto', 10);

function barker_horario_reparto($checkout)
{

	// get_template_part( 'templates/select-distric' );

	woocommerce_form_field('f_shipping_address', array(
		'type' => 'select',
		'class' => array(
			'lms-drop form-row-wide'
		) ,
		'label' => __('Dirección de envío') ,
		'required' => true,
		'options' => array(
			'blank' => __('Distrito', 'barker_distrito') ,
			'Barranco' => __('Barranco', 'barker_distrito') ,
			'Chorrillos' => __('Chorrillos', 'barker_distrito') ,
			'Jesús María' => __('Jesús María', 'barker_distrito') ,
			'La Molina' => __('La Molina', 'barker_distrito') ,
			'Lince' => __('Lince', 'barker_distrito') ,
			'Miraflores' => __('Miraflores', 'barker_distrito') ,
			'San Borja' => __('San Borja', 'barker_distrito') ,
			'San Isidro' => __('San Isidro', 'barker_distrito') ,
			'Surco' => __('Surco', 'barker_distrito') ,
			'Surquillo' => __('Surquillo', 'barker_distrito')
		)
	) , $checkout->get_value('f_shipping_address'));
	echo '
			<div id="barker_horario_reparto" data-priority="45">
			
				';
	woocommerce_form_field_radio('barker_horario_reparto', array(
		'type' => 'select',
		'priority' => 45,
		'class' => array(
			'here-about-us form-row-wide'
		) ,
		'label' => __('') ,
		'placeholder' => __('') ,
		'required' => true,
		'options' => array(
			'9am a 12pm' => '9am a 12pm<br/><span class="barker_item_horario">Turno día</span>',
			'1pm a 6pm' => '1pm a 6pm<br/><span class="barker_item_horario">Turno tarde</span>'
		)
	) , $checkout->get_value('barker_horario_reparto'));
	echo '
			</div>
			';
}

// ADD CUSTOM INPUT RADIO BILLING FIELD ON CHECKOUT --> DELIVERY TIME
// -----------------------------------------------------------------

add_action('woocommerce_after_checkout_billing_form', 'barker_checkout_kind_house', 10);

function barker_checkout_kind_house($checkout) {
	

	// ADD CUSTOM FIELD TO CHECKOUT --> ORDER ID PAYMENT
	// --------------------------------------------------

	woocommerce_form_field('barker_order_payment_id', array(
		'type' => 'text',
		'required' => false,
		'placeholder' => _x('Order ID', 'placeholder', 'woocommerce') ,
		'class' => array(
			'lms-drop form-row-wide'
		) ,
		'label' => __('Order ID')
	) , $checkout->get_value('barker_order_payment_id'));
	
	
	// ADD CUSTOM FIELD TO CHECKOUT --> KIND HOUSE
	// --------------------------------------------

	echo '
			<div id="barker_checkout_kind_house">
				
				';
	woocommerce_form_field_radio_house('barker_checkout_kind_house', array(
		'type' => 'select',
		'priority' => 45,
		'id' => '',
		'class' => array(
			'here-about-us form-row-wide'
		) ,
		'label' => __('') ,
		'placeholder' => __('') ,
		'required' => true,
		'options' => array(
			'Casa' => 'Casa<br/>',
			'Quinta' => 'Quinta<br/>',
			'Edificio' => 'Edificio<br/>',
			'Condominio' => 'Condominio<br/>'
		)
	) , $checkout->get_value('barker_checkout_kind_house'));
	echo '
			</div>
			';
}

// PROCESSS VALIDATION TO CHECKOUT --> DELIVERY TIME
// --------------------------------------------------
add_action('woocommerce_checkout_process', 'my_custom_checkout_field_process');

function my_custom_checkout_field_process()
{
	global $woocommerce;

	// Check if set, if its not set add an error.

	if (!$_POST['barker_horario_reparto']) $woocommerce->add_error(__('Por favor selecciona un horario de reparto.'));
}

// REGISTER CUSTOM FIELD TO ORDER WOOCOMMERCE --> DELIVERY TIME
// -------------------------------------------------------------
add_action('woocommerce_checkout_update_order_meta', 'barker_horario_reparto_field_update_order_meta');

function barker_horario_reparto_field_update_order_meta($order_id)
{
	if ($_POST['barker_horario_reparto']) update_post_meta($order_id, 'Horario de reparto', esc_attr($_POST['barker_horario_reparto']));
}


// PROCESSS VALIDATION TO CHECKOUT --> DELIVERY TIME
// --------------------------------------------------
add_action('woocommerce_checkout_process', 'my_custom_checkout_field_process_casa');

function my_custom_checkout_field_process_casa()
{
	global $woocommerce;

	// Check if set, if its not set add an error.

	if (!$_POST['barker_checkout_kind_house']) $woocommerce->add_error(__('Por favor selecciona un tipo de casa.'));
}

// REGISTER CUSTOM FIELD TO ORDER WOOCOMMERCE --> KIND HOUSE
// --------------------------------------------------
add_action('woocommerce_checkout_update_order_meta', 'barker_checkout_kind_house_field_update_order_meta_casa');

function barker_checkout_kind_house_field_update_order_meta_casa($order_id)
{
	if ($_POST['barker_checkout_kind_house']) update_post_meta($order_id, 'Tipo de Casa', esc_attr($_POST['barker_checkout_kind_house']));
}

// REGISTER CUSTOM FIELD TO ORDER WOOCOMMERCE --> ORDER PAYMENT ID
// --------------------------------------------------

add_action('woocommerce_checkout_update_order_meta', 'barker_order_payment_id');

function barker_order_payment_id($order_id)
{
	if ($_POST['barker_order_payment_id']) update_post_meta($order_id, 'barker_order_payment_id', esc_attr($_POST['barker_order_payment_id']));
}


// PROCESSS VALIDATION TO CHECKOUT --> DISTRICT
// --------------------------------------------------
add_action('woocommerce_checkout_process', 'my_custom_checkout_field_process_address');

function my_custom_checkout_field_process_address()
{
	global $woocommerce;

	// Check if set, if its not set add an error.

	if (!$_POST['f_shipping_address']) $woocommerce->add_error(__('Por favor selecciona un distrito.'));
}


// REGISTER CUSTOM FIELD TO ORDER WOOCOMMERCE --> DISTRICT
// -------------------------------------------------------
add_action('woocommerce_checkout_update_order_meta', 'f_shipping_address_field_update_order_meta_address');

function f_shipping_address_field_update_order_meta_address($order_id)
{
	if ($_POST['f_shipping_address']) update_post_meta($order_id, 'Distrito', esc_attr($_POST['f_shipping_address']));
}



// ADITINAL FIELD TO WOOCOMMERCE CHECKOUT --> WOOCOMMERCE COUPON
// --------------------------------------------------------------

add_action('woocommerce_checkout_after_terms_and_conditions', 'barker_checkout_coupon');

function barker_checkout_coupon()
{
	echo '<form id="wooCouponCode" class="barker-checkout-customCoupon-box checkout_coupon woocommerce-form-coupon" method="post" style="display:none">

		<p>If you have a coupon code, please apply it below.</p>

		<p class="form-row form-row-first">
			<input type="text" name="coupon_code" class="input-text" placeholder="Coupon code" id="coupon_code" value="">
		</p>

		<p class="form-row form-row-last">
			<button type="submit" class="button" name="apply_coupon" value="Apply coupon">Apply coupon</button>
		</p>

		<div class="clear"></div>
	</form>';
}

add_action('woocommerce_checkout_order_review', 'barker_checkout_header_title');




function barker_checkout_header_title()
{
	echo '
	<div class="order-detail__head u-text-center">
		<div class="subtitle-xs">
	    	<h4>Resumen del pedido</h4>
		</div>
		<div class="order-item__head u-text-center">
	    	<h4>Caja de <span class="js-dog-name">tu mascota</span></h4>
	    </div>
	</div>
	
	';
	echo '
<div class="order-detail__row barker-checkout-date-box">
    <dl>
        <dt>Fecha de entrega estimada</dt>
        <dd>Mié, 04/25</dd>
    </dl>
</div>';
	echo '<div class="order-detail__row_notice u-not-border u-text-center barker-checkout-notice-cancel">
        <p class="feedback">Puedes cancelar la suscripción en<br />cualquier momento.</p>
    </div>';
}

// CHANGUE BUTTON PLACE ORDER WOOCOMMERCE CHECKOUT 
// -----------------------------------------------

add_filter('woocommerce_order_button_text', 'woo_custom_order_button_text');

function woo_custom_order_button_text()
{
	return __('Realizar Pedido', 'woocommerce');
}

// CHANGUE TITLE BILLING FIELDS WOOCOMMERCE CHECKOUT 
// -------------------------------------------------

function wc_billing_field_strings($translated_text, $text, $domain)
{
	switch ($translated_text) {
	case 'Billing details':
		$translated_text = __('Complete sus datos para comenzar su prueba', 'woocommerce');
		break;
	}

	return $translated_text;
}

add_filter('gettext', 'wc_billing_field_strings', 20, 3);
add_action('woocommerce_before_checkout_billing_form', 'barker_checkout_header_top');


// ADD SUBTITLE BILLING FIELDS WOOCOMMERCE CHECKOUT 
// -------------------------------------------------

function barker_checkout_header_top()
{
	echo '<p>Estás tan cerca de la comida real, ¡Tu perro puede olerla!</p>';
}

add_action('woocommerce_before_checkout_form', 'andina_software_barker_checkout_steps_header');

// ADD PAYFORM FOR CULQI 
// ---------------------

function andina_software_barker_checkout_steps_header()
{
	get_template_part('/templates/steps-header');
}

// ADD PAYFORM FOR CULQI 
// ---------------------

add_action('woocommerce_checkout_after_customer_details', 'andina_software_barker_culqipayform');

function andina_software_barker_culqipayform()
{
	get_template_part('/templates/culqi-payform');
}

// ADD STANDAR PAYFORM LAYOUT 
// --------------------------

add_action('woocommerce_checkout_before_customer_details', 'andina_software_mirror');

function andina_software_mirror()
{
	get_template_part('/templates/subscription');
}

