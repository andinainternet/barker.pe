jQuery(document).ready(function ($) {

	//Init Price Box
	//--------------

	jQuery('#barkerPriceBox-pollo').removeClass('barker-suscription-item_priceBox__disable');
	jQuery('#barkerPriceBox-pollo').addClass('barker-suscription-item_priceBox__active');


	//BOX SELECTION
	//--------------

	jQuery('[name="f-flavors"]').click(function () {
		var barker_radio_value = this.value;

		var barker_description_box = '#barkerDescription-';
		var barker_price_box = '#barkerPriceBox-';
		var barker_description_dynamicID = barker_description_box + barker_radio_value;
		var barker_priceBox_dynamicID = barker_price_box + barker_radio_value;


		console.log(barker_radio_value);
		// alert(barker_priceBox_dynamicID);

		jQuery('.delivery-flavors__feedback').addClass('barker-suscription-item_description__disable');
		jQuery('.owl-stage-outer').addClass('barker-suscription-item_description__disable');
		jQuery('.filter-by-time').addClass('barker-suscription-item_priceBox__disable');

		//Description Box
		jQuery(barker_description_dynamicID).removeClass('barker-suscription-item_description__disable');
		jQuery(barker_description_dynamicID).addClass('barker-suscription-item_description__active');

		//Price Box
		jQuery(barker_priceBox_dynamicID).removeClass('barker-suscription-item_priceBox__disable');
		jQuery(barker_priceBox_dynamicID).addClass('barker-suscription-item_priceBox__active');


	});

	// DYNAMIC URL
	//--------------
	jQuery('[name="f-suscription-time"]').click(function () {

		var barker_product_id = jQuery('[name="f-flavors"]:checked').data('id');
		var barker_suscriptionTime_attr_id = jQuery(this).data('variation');
		var barker_suscriptionTime_qty = jQuery(this).data('quantity');

		// alert('el id es' + barker_product_id + 'y el variation id es ' + barker_suscriptionTime_attr_id);

		var barker_baseUrl = document.location.origin;
		var barker_addCart = '?add-to-cart=';
		var barker_variationID = '&variation_id=';
		var barker_quantity = '&quantity=';

		barker_fullCheckoutURL = barker_baseUrl + barker_addCart + barker_product_id + barker_variationID + barker_suscriptionTime_attr_id + barker_quantity + barker_suscriptionTime_qty;

		// alert(barker_fullCheckoutURL);

		var barker_submit = jQuery('a[href*="#next"]');

		jQuery(barker_submit).attr('href', barker_fullCheckoutURL);

		var barker_reference_button = jQuery('#delivery-wizard > .actions > ul > li:nth-child(1)');

		var barker_remove_button = jQuery('#delivery-wizard > .actions > ul > li:nth-child(2)');
		var barker_button_checkout = '<li><a id="barker_goToCheckout_url" href="#">Continuar</a></li>';

		jQuery(barker_remove_button).click(function () {

			window.location.href = barker_fullCheckoutURL;
			console.log(barker_fullCheckoutURL);
		})


	});


	//BARKER CHECKOUT CUSTOM FIELDS
	//-----------------------------

	var billing_state_field = jQuery('#billing_state_field');
	var billing_city_field = jQuery('#billing_city_field');
	var barker_horario_reparto = jQuery('#barker_horario_reparto');
	var f_shipping_address_field = jQuery('#f_shipping_address_field');
	var barker_checkout_kind_house = jQuery('#barker_checkout_kind_house');

	jQuery(barker_horario_reparto).insertAfter(billing_state_field);
	jQuery(barker_checkout_kind_house).insertAfter(billing_city_field);
	jQuery(f_shipping_address_field).insertAfter(billing_city_field);

	//BARKER ORDER REVIEW
	//-------------------

	var payment = jQuery('#payment');
	var checkout_coupon = jQuery('.checkout_coupon');

	//RADIO ACTIVE
	//--------------

	var barker_checkout_radio_house_container = jQuery('.barker-checkout-radio-house-container > input[type="radio"]');

	jQuery(barker_checkout_radio_house_container).click(function () {

		var barker_radio_parent = jQuery(this).parent();
		var barker_radio_container = jQuery('.barker-checkout-radio-house-container');

		jQuery(barker_radio_container).removeClass('barker-checkout-radio-house-container-active')
		jQuery(barker_radio_parent).addClass('barker-checkout-radio-house-container-active');

		console.log(this.value);
	});


	//Value Checkout
	//--------------

	jQuery(function () {

		var f_first_name = jQuery('#f-first-name');
		var f_last_name = jQuery('#f-last-name');
		var f_telephone = jQuery('#f-telephone');


		var f_address_1 = jQuery('#f-address-1');
		var f_address_2 = jQuery('#f-address-2');
		var f_address_3 = jQuery('#f-address-3');

		var f_shipping_address_mask = jQuery('#f-shipping-address-mask');

		var woo_coupon = jQuery('#wooCouponCode');

		jQuery(woo_coupon).addClass('hola');

		var barker_billing_first_name = jQuery('#billing_first_name');
		var barker_billing_last_name = jQuery('#billing_last_name');
		var barker_billing_phone = jQuery('#billing_phone');

		var barker_billing_address_1 = jQuery('#billing_address_1');
		var barker_billing_address_2 = jQuery('#billing_address_2');
		var barker_billing_city = jQuery('#billing_city');

		var barker_shipping_address_mask = jQuery('#f_shipping_address');

		var barker_coupon = jQuery('#barker_coupon');

		var barker_coupon_btn = jQuery('#barker_coupon_btn');

		var barker_checkout_submit = jQuery('#barker_checkout_submit');

		jQuery(barker_coupon).change(function () {
			/* Act on the event */

			jQuery('#order_review').find('#wooCouponCode > .form-row > .input-text').val(barker_coupon.val());

			jQuery('.order-detail').find(barker_coupon_btn).removeClass('is-disabled');
			jQuery('.order-detail').find(barker_coupon_btn).addClass('is-active');

		});


		jQuery(barker_coupon_btn).click(function () {

			var woo_button_coupon = jQuery('#wooCouponCode > .form-row > .button');

			jQuery(woo_button_coupon).trigger('click');

		});


		jQuery(barker_checkout_submit).click(function () {

			var woo_button_submit = jQuery('#place_order');
			var woo_button_submit_2 = jQuery('#place_order_2');


			jQuery(woo_button_submit).trigger('click');
			jQuery(woo_button_submit_2).trigger('click');

		});


		function onChange1() {
			barker_billing_first_name.val(f_first_name.val());
		};

		function onChange2() {
			barker_billing_last_name.val(f_last_name.val());
		};

		function onChange3() {
			barker_billing_phone.val(f_telephone.val());
		};

		function onChange4() {
			barker_billing_address_1.val(f_address_1.val());
		};

		function onChange5() {
			barker_billing_address_2.val(f_address_2.val());
		};

		function onChange6() {
			barker_billing_city.val(f_address_3.val());
		};

		function onChange7() {
			barker_shipping_address_mask.val(f_shipping_address_mask.val());
		};


		jQuery(f_first_name)
			.change(onChange1)
			.keyup(onChange1);

		jQuery(f_last_name)
			.change(onChange2)
			.keyup(onChange2);

		jQuery(f_telephone)
			.change(onChange3)
			.keyup(onChange3);

		jQuery(f_address_1)
			.change(onChange4)
			.keyup(onChange4);

		jQuery(f_address_2)
			.change(onChange5)
			.keyup(onChange5);

		jQuery(f_address_3)
			.change(onChange6)
			.keyup(onChange6);

		jQuery(f_shipping_address_mask)
			.change(onChange7)
			.keyup(onChange7);


		var woo_producto_simple = jQuery('.cart_item > .product-name').html();
		var woo_precio_simple = jQuery('tbody .cart_item > .product-total > .woocommerce-Price-amount').text();
		var woo_precio_total_simple = jQuery('.order-total > td > strong > .woocommerce-Price-amount').text();
		var woo_discount_simple = jQuery('.cart-discount > th').text;
		var woo_terms = jQuery('.woocommerce-form__label-for-checkbox');
		var barker_producto = jQuery('.order-item__text-desc');
		var barker_precio = jQuery('.order-item__text-amount');
		var barker_precioTotal = jQuery('.barker-precioTotal');
		var barker_terms = jQuery('#barker_terms');
		var barker_checkbox_terms = jQuery('.checkbox-terms > .icon');
		var barker_title_discount = jQuery('.barker-title-discount');
		var barker_price_discount = jQuery('.barker-price-discount');

		
		// APLY CHANGUE AFTER LOAD AJAX ORDER REVIEW WOOCOMMERCE CHECKOUT
		// --------------------------------------------------------------

		jQuery('body').on('updated_checkout', function () {

			// jQuery('.order-detail').find(barker_coupon_btn).removeClass('is-disabled');
			jQuery('#order_review').find('.cart_item > .product-name').css('border', 'red');

			var woo_producto = jQuery('.cart_item > .product-name');
			var woo_precio = jQuery('tbody .cart_item > .product-total > .woocommerce-Price-amount');
			var woo_precio_total = jQuery('.order-total > td > strong > .woocommerce-Price-amount');
			var woo_discount = jQuery('.cart-discount > th');
			var woo_discount_price = jQuery('.cart-discount > td > .woocommerce-Price-amount');

			jQuery(woo_producto).clone().prependTo(barker_producto);

			jQuery(woo_precio).clone().prependTo(barker_precio);

			jQuery(woo_precio_total).clone().prependTo(barker_precioTotal);

			jQuery(woo_discount).clone().prependTo(barker_title_discount);

			jQuery(woo_discount_price).clone().prependTo(barker_price_discount);

			jQuery(barker_checkbox_terms).click(function () {
				jQuery('#order_review').find('.woocommerce-form__label-for-checkbox').trigger('click');
			});


		});


	});


	var agregar_mascota = jQuery('.js-add-dog');
	var barker_horario_morning = jQuery('#f-moorning-1');
	var barker_horario_afternoon = jQuery('#f-afternoon-1');	
	var barker_casa_house = jQuery('#f-house');
	var barker_casa_fifth = jQuery('#f-fifth');
	var barker_casa_building = jQuery('#f-building');
	var barker_casa_condominium = jQuery('#f-condominium');	

	jQuery(agregar_mascota).click(function () {
		window.location.href = document.location.origin + '/suscripcion';
	});

	jQuery(barker_horario_morning).click(function () {
		var woo_horario_reparto = jQuery('#barker_horario_reparto > .barker-checkout-radio-container:nth-child(1) > input');
		jQuery(woo_horario_reparto).trigger('click');

	})

	jQuery(barker_horario_afternoon).click(function () {
		var woo_horario_reparto = jQuery('#barker_horario_reparto > .barker-checkout-radio-container:nth-child(2) > input');
		jQuery(woo_horario_reparto).trigger('click');

	})

	jQuery(barker_casa_house).click(function () {
		var woo_casa_tipo = jQuery('#barker_checkout_kind_house > .barker-checkout-radio-house-container:nth-child(1) > input');
		jQuery(woo_casa_tipo).trigger('click');

	})

	jQuery(barker_casa_fifth).click(function () {
		var woo_casa_tipo = jQuery('#barker_checkout_kind_house > .barker-checkout-radio-house-container:nth-child(2) > input');
		jQuery(woo_casa_tipo).trigger('click');

	})

	jQuery(barker_casa_building).click(function () {
		var woo_casa_tipo = jQuery('#barker_checkout_kind_house > .barker-checkout-radio-house-container:nth-child(3) > input');
		jQuery(woo_casa_tipo).trigger('click');

	})

	jQuery(barker_casa_condominium).click(function () {
		var woo_casa_tipo = jQuery('#barker_checkout_kind_house > .barker-checkout-radio-house-container:nth-child(4) > input');
		jQuery(woo_casa_tipo).trigger('click');

	})


});