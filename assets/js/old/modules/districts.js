// JSON de distritos:
export default  [
  {
    name: 'Barranco',
    availableDays: ['Martes', 'Sábado']
  },
  {
    name: 'Chorrillos',
    availableDays: ['Martes', 'Sábado']
  },
  {
    name: 'Jesús María',
    availableDays: ['Miércoles', 'Viernes']
  },
  {
    name: 'La Molina',
    availableDays: ['Lunes', 'Jueves']
  },
  {
    name: 'Lince',
    availableDays: ['Miércoles', 'Viernes']
  },
  {
    name: 'Miraflores',
    availableDays: ['Martes', 'Sábado']
  },
  {
    name: 'San Borja',
    availableDays: ['Miércoles', 'Viernes']
  },
  {
    name: 'San Isidro',
    availableDays: ['Miércoles', 'Viernes']
  },
  {
    name: 'San Miguel',
    availableDays: ['Miércoles', 'Viernes']
  },
  {
    name: 'Surco',
    availableDays: ['Lunes', 'Jueves']
  },
  {
    name: 'Surquillo',
    availableDays: ['Martes', 'Sábado']
  },
]