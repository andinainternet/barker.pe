import 'jquery-steps';
import wNumb from '../vendor/w-numb';
import noUiSlider from 'nouislider';
// import select2 from 'select2';
import moment from 'moment';
import 'bootstrap4-datetimepicker';
import 'moment-locales'

/* Our data */
import dataFlavors from './data-flavors';
import districts from './districts';

var s, d,
delivery = {
	$flavorsCarousel: '',
	dogName: '',
	flavors: '',
	orderList: [],
	arrowLeft:'<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 284.935 284.936"><path d="M110.488 142.468L222.694 30.264c1.902-1.903 2.854-4.093 2.854-6.567s-.951-4.664-2.854-6.563L208.417 2.857C206.513.955 204.324 0 201.856 0c-2.475 0-4.664.955-6.567 2.857L62.24 135.9c-1.903 1.903-2.852 4.093-2.852 6.567 0 2.475.949 4.664 2.852 6.567l133.042 133.043c1.906 1.906 4.097 2.857 6.571 2.857 2.471 0 4.66-.951 6.563-2.857l14.277-14.267c1.902-1.903 2.851-4.094 2.851-6.57 0-2.472-.948-4.661-2.851-6.564L110.488 142.468z"/></svg>',
	arrowRight: '<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 284.935 284.936"><path d="M222.701 135.9L89.652 2.857C87.748.955 85.557 0 83.084 0c-2.474 0-4.664.955-6.567 2.857L62.244 17.133c-1.906 1.903-2.855 4.089-2.855 6.567 0 2.478.949 4.664 2.855 6.567l112.204 112.204L62.244 254.677c-1.906 1.903-2.855 4.093-2.855 6.564 0 2.477.949 4.667 2.855 6.57l14.274 14.271c1.903 1.905 4.093 2.854 6.567 2.854 2.473 0 4.663-.951 6.567-2.854l133.042-133.044c1.902-1.902 2.854-4.093 2.854-6.567s-.945-4.664-2.847-6.571z"/></svg>',
	
	settings: {},

	init: function() {
		d = this;
		s = this.settings;
		this.initWizard();
		this.bindEvents();
		this.previewImage();
		this.calculateSizePet();
		this.chooseFlavor();
		this.orderForAWeek();
		this.initSuscriptionCarousel();
		this.filterBySex();
		this.filterByStatusFemale();
		this.getDogName();
		this.selectDistrict();
		this.addDog();
		this.addDistrict();
		// this.addDistrictSearch();
	},

	bindEvents: function() {
		$('input[name="f-suscription-time"]').on('change', d.orderForAWeek);
	},

	initWizard: function() {	
		var stepsWizard;

		$('#delivery-wizard').steps({
	    headerTag: 'h3',
			bodyTag: 'section',
			// transitionEffect: "slideLeft",
    	autoFocus: true,
	    // autoFocus: true,
	    // forceMoveForward: true,
	    labels: {
	    	finish: "Realizar pedido",
        next: "Continuar",
        previous: "Regresar",
	    },
	    onStepChanging: function (event, currentIndex, newIndex) {
	    	return true;
	    },
	    onStepChanged: function (event, currentIndex, priorIndex) {
	    	
	    },
	    onFinishing: function (event, currentIndex) {
        
	    },
	    onFinished: function (event, currentIndex) {
	    	
	    	//delivery.sendFormQuote();
	    }
	    // enablePagination: false
		});		
	},

	sendFormQuote: function () {
		
	},

	addDistrict: function() {
		$('.js-add-district-popup').on('click', (e) => {
			e.preventDefault();

			$.magnificPopup.instance._onFocusIn = function(e) {
				// Do nothing if target element is select2 input
				if( $(e.target).hasClass('select2-search__field') ) {
						return true;
				} 
				// Else call parent method
				$.magnificPopup.proto._onFocusIn.call(this,e);
		};

			$.magnificPopup.open({
				// focus: '#f-district-search-add',
				items: {
					src: '.js-add-district',
					type: 'inline'
				},
				// callbacks: {
				// 	open:	() => {
				// 		d.addDistrictSearch();
				// 	}
				// }
			});

			$('.js-add-district-select').select2({
				placeholder: 'Busca tu distrito',
				// allowClear: true,
				width: "100%"
			});
		});
	},

	addDistrictSearch: function() {
		console.log('open now');
		$('.js-add-district-select').select2({
			placeholder: 'Busca tu distrito',
			// allowClear: true,
			width: "100%"
		});
	},

	previewImage: function() {
		let $imgTarget = $('.file-upload .js-upload-hidden');
		
		$imgTarget.on('change', (e) => {
			let $this = $(e.currentTarget);
			let $preview = $this.parent().prev();

			if (window.FileReader) {
				if (!$this[0].files[0].type.match(/image\//)) return;

				let reader = new FileReader();
				reader.onload = function(e) {
					let src = e.target.result;
					console.log(src);				
					$preview.find('img').attr('src', src);
				}
				reader.readAsDataURL($this[0].files[0]);
			} else {
				$this[0].select();
				$this[0].blur();
				
			}
		});
	},

	filterBySex: function() {
		let $inputSex = $('input[name="f-dog-sex"]');

		$inputSex.on('change', (e) => {
			let id = $(e.currentTarget).attr('id');
			
			if (id == 'f-female') {
				$('.filter-female').fadeIn('fast');
			} else {
				$('.filter-female').fadeOut('fast');
			}
		});
	},

	filterByStatusFemale: function() {
		// let $inputStatusFemale = $('input[name="f-status-female"]');

		// $inputStatusFemale.on('change', (e) => {
		// 	let id = $(e.currentTarget).attr('id');
				
		// 	if (id == 'f-status-na') {
		// 		$('.email-custom-recipe')
		// 			.fadeIn('fast')
		// 			.find('input').focus();
				
		// 	} else {
		// 		$('.email-custom-recipe').fadeOut('fast');
		// 	}
		// });
	},

	calculateSizePet: function() {
		let sizePetSlider = $('#slider-size-dog')[0];
		let $dogWeight  = $('.js-dog-weight');
		let $dogName = $('.js-dog-race');
		let $dogImage = $('.js-dog-image');
		let $dogLess = $('.js-dog-less');
		let $dogHigher = $('.js-dog-higher');

		var pipCustom = {};
		pipCustom['0'] = 'Menor';
		pipCustom['12'] = 'Mayor';

		var dogs = [
			{
				race: 'Chihuahua',
				category: 'Categoría 1',
				weight: '1 - 5kg'
			},
			{
				race: 'Yorkshire',
				category: 'Categoría 1',
				weight: '1 - 5kg'
			},
			{
				race: 'Bichon frise',
				category: 'Categoría 1',
				weight: '1 - 5kg'
			},
			{
				race: 'Jack russell terrier',
				category: 'Categoría 2',
				weight: '5 - 10 kg'
			},
			{
				race: 'Shitzu',
				category: 'Categoría 2',
				weight: '5 - 10 kg'
			},
			{
				race: 'Perro salchicha',
				category: 'Categoría 2',
				weight: '5 - 10 kg'
			},
			{
				race: 'Beagle',
				category: 'Categoría 3',
				weight: '10 - 22kg'
			},
			{
				race: 'Schnauzer',
				category: 'Categoría 3',
				weight: '10 - 22kg'
			},
			{
				race: 'Bulldog francés',
				category: 'Categoría 3',
				weight: '10 - 22kg'
			},
			{
				race: 'Sharpei',
				category: 'Categoría 3',
				weight: '10 - 22kg'
			},
			{
				race: 'Pastor alemán',
				category: 'Categoría 4',
				weight: '22 - 45kg'
			},
			{
				race: 'Golden retriever',
				category: 'Categoría 4',
				weight: '22 - 45kg'
			},
			{
				race: 'Doberman',
				category: 'Categoría 4',
				weight: '22 - 45kg'
			}
		]

		if (sizePetSlider) {
			noUiSlider.create(sizePetSlider, {
				start: Math.floor(dogs.length / 2),
				step: 1,
				format: wNumb({
					decimals: 0
				}),
				range: {
					'min': 0,
					'max': dogs.length - 1
				},
				pips: {
					mode: 'range',
					// values: [0, 13],
					density: -1,
					format: {
						to: function(a) {
							return pipCustom[a];
						}
					}
				}
			});
	
			$dogLess.find('img').attr('src', `./images/dogs/${dogs[0].race.toLowerCase().replace(/ /g, '-')}.png`);
			$dogHigher.find('img').attr('src', `./images/dogs/${dogs[dogs.length-1].race.toLowerCase().replace(/ /g, '-')}.png`);
	
			sizePetSlider.noUiSlider.on('update', (values, handle) => {
				let obj = dogs[values[handle]];
	
				$dogWeight.html(`${obj.category} (${obj.weight})`);
				$dogName.html(obj.race);
				$dogImage.find('img').attr('src', `./images/dogs/${obj.race.toLowerCase().replace(/ /g, '-')}.png`);
			});
		}
	},

	getObjectInArray: function(data, val) {
		let result = null;
		data.map((item) => {
			if (item['name'] == val) result = item;
		});
		return result;
	},

	chooseFlavor: function() {
		d.$flavorsCarousel = $('.js-delivery-flavors-carousel');

		$('input[name="f-flavors"]').on('change', (e) => {
			let $this = $(e.currentTarget);
			let flavorName = $this.data('flavor'); // get flavor
			let flavorData = d.getObjectInArray(dataFlavors, flavorName); // get data of flavor

			d.addFlavorItem($this, flavorName, flavorData);
		});
	},

	initFlavorsCarousel: function() {
		d.$flavorsCarousel.owlCarousel({
			items: 1,
			nav: true,
			navText: [d.arrowLeft, d.arrowRight]
		});
	},

	initSuscriptionCarousel: function() {
		const isTablet = window.matchMedia('(max-width: 991px)');

		if (isTablet.matches) {
			$('.js-suscription-type-carousel').owlCarousel({
				items: 1,
				stagePadding: 25
			});
		}
	},

	orderForAWeek: function() {
		let $oneWeekTime = $('#f-one-week');
		let $flavorMixto = $('#f-mixed-flavor');

		if ($oneWeekTime.length && $oneWeekTime.is(':checked')) {
			if ($flavorMixto.is(':checked')) {
				d.destroyFlavorsCarousel();
				d.flavors = '';
			}
			$flavorMixto
				.prop('disabled', true)
				.prop('checked', false);
		} else {
			$flavorMixto.prop('disabled', false);
		}
	},

	addFlavorItem: function(current, flavorName, flavorData) {
		if (!d.$flavorsCarousel.children().length) {
			d.$flavorsCarousel.closest('.delivery-flavors').removeClass('is-empty');
			d.initFlavorsCarousel();
		}

		d.flavors = flavorName;
		d.$flavorsCarousel
			.trigger('remove.owl.carousel', 0)
			.trigger('add.owl.carousel', [d.tplFlavor(flavorData), 0])
			.trigger('refresh.owl.carousel');
	},

	destroyFlavorsCarousel: function() {
		d.$flavorsCarousel.closest('.delivery-flavors').addClass('is-empty');

		for (let i = 0; i < $('.owl-item').length; i++) {
			d.$flavorsCarousel
				.trigger('destroy.owl.carousel')
				.children().remove();
		}
	},

	tplFlavor: function(flavorData) {
		let tpl = `<div class="delivery-flavor">
			<figure class="delivery-flavor__image"><img src="./images/productos/${flavorData.image}.png" alt=""></figure>
			<div class="delivery-flavor__title subtitle-sm">
				<h3>${flavorData.name}</h3>
			</div>
			<div class="delivery-flavor__content">
				${flavorData.text}
			</div>
			<div class="delivery-flavor__cta"><a class="btn btn--primary btn--block" href="">Ver cuadro nutricional</a></div>
		</div>`;

		return tpl;
	},

	getDogName: function() {
		let $dogNamePrint = $('.js-dog-name');

		$('#f-dog-name').keyup((e) => {
			let val = $(e.currentTarget).val();
			d.dogName = val;
			console.log(d.dogName);
			
			
			if (val != '') {
				$dogNamePrint.html(val);
			} else {
				$dogNamePrint.html('tu mascota');
			}
		});
	},

	selectDistrict: function() {
		let $shippingAddress = $('#f-shipping-address');
		let $infoDateInput = $('#f-info-date');
		$infoDateInput.hide();

		$shippingAddress.on('change', (e) => {
			let val = $(e.currentTarget).val();

			if (val != '') {
				let objDistrict = d.getObjectInArray(districts, val);
				d.updatePicker(objDistrict.availableDays);
			} else {
				$infoDateInput.fadeOut('fast');
			}
		});
	},

	updatePicker: function(availableDays) {
		// let $infoDate = $('.js-info-date');
		// let days = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
		// let indexDays = [0, 1, 2, 3, 4, 5, 6];
		let $infoDateInput = $('#f-info-date');
		let textDays = [];

		availableDays.map((el) => {
			// let i = days.indexOf(el);
			textDays.push(el);
			// days.splice(i, 1);
			// indexDays.splice(i, 1);
		});

		$infoDateInput.val(`Días de entrega: ${textDays.join(' y ')}`);
		$infoDateInput.fadeIn('fast');
		
		// Update Datetimepicker options
		// $('#f-delivery-date').data('DateTimePicker').daysOfWeekDisabled(indexDays);
	},

	addDog: function() {
		const $addDog = $('.js-add-dog');
		const $btnConfirmSimilarInfo = $('.js-confirm-similar-info').find('.btn');

		$addDog.on('click', (e) => {
			d.saveOrder();
			console.log(d.orderList);
		});
		// const $addDog = $('.js-add-dog');
		// const $btnConfirmSimilarInfo = $('.js-confirm-similar-info').find('.btn');

		// // Show modal that confirm similar info the dog
		// $addDog.on('click', (e) => {
		// 	$.magnificPopup.open({
		// 		items: {
		// 			src: '#add-dog-new',
		// 			type: 'inline'
		// 		}
		// 	});
		// });

		// $btnConfirmSimilarInfo.on('click', (e) => {
		// 	let similarInfo = $(e.currentTarget).data('similar-info');
		// 	location.reload();

		// 	if (similarInfo == 'yes') {
				
		// 	}
		// });
	},
	
	saveOrder: function() {
		let order = {
			indexAvatar: 1,
			dogName: $('#f-dog-name').val(),
			dogSex: $('input[name="f-dog-sex"]:checked').val(),
			dogStatusFelamel: $('input[name="f-status-female"]:checked').val(),
			dogAge: $('input[name="f-dog-age"]:checked').val(),
			dogActivity: $('input[name="f-level-activity"]:checked').val(),
			flavor: $('input[name="f-flavors"]:checked').val(),
			suscriptionType: $('input[name="f-suscription-time"]:checked').val(),

			
		}

		d.orderList.push(order);
	}
}

export default delivery;