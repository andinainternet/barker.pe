export default [
  {
    district: 'Miraflores',
    local: 'Animal Medical Center',
    address: 'Av. Gral. Vidal',
    phone: '4558900',
    coords: [-33.890542, 151.274856]
  },
  {
    district: 'Santiago de Surco',
    local: 'Animal Medical Center',
    address: 'Av. Santiago de Surco 151',
    phone: '4663022',
    coords: [-33.923036, 151.259052]
  },
  {
    district: 'Surquillo',
    local: 'Animal Medical Center',
    address: 'Av. Dante 1110',
    phone: '5669090',
    coords: [-34.028249, 151.157507]
  },
  {
    district: 'Barranco',
    local: 'Animal Medical Center',
    address: 'Av. Almirantes Grau',
    phone: '4001001',
    coords: [-33.80010128657071, 151.28747820854187]
  },
  {
    district: 'Jesus María',
    local: 'Animal Medical Center',
    address: 'Av. Almirantes Grau',
    phone: '5030122',
    coords: [-33.950198, 151.259302]
  }
]